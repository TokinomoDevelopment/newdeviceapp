import threading
import ntplib
from subprocess import Popen, PIPE
from time import sleep
import requests
from subprocess import check_output
import serial
import time
import json 
import os  
import socket

from devicelogger import DeviceLogger
from device.controller import DeviceController
from status import Status
from const import Constants


class NetworkManager(object):

    def __init__(self, callback, err):
        self.args_status = False
        self.callback = callback
        self.err = err
        self.app_url = Status.get_url()
        self.auth_headers =  { 'X-API-KEY': Constants.api_key , \
            'User-Agent' : 'Tokinomo_{0}/v{1}{2}'.format(Status.get_device_id(), Status.get_app_version(), Status.get_libs())}
        self.timer = Constants.short_time # default timer 
        self.modem_connected = False 
        self.pool_timer = None
        self.time = None # Pool timer remaining time
        self.monitor_cloud_connection()
        DeviceLogger.log('info', 'NetworkManager', 'Initialised NetworkManager')


    def start(self):
        self.worker = threading.Thread(target=self.run, name="Establish Connection Thread")
        self.worker.running = True
        self.worker.start()

    def stop(self):
        self.worker.running = False

    def run(self):
        try:
            t = threading.currentThread()
            while getattr(t, "running", True):
                DeviceLogger.log('info', 'NetworkManager', '............START............')  
                DeviceLogger.log('info', 'NetworkManager', 'Trying WiFi connection')    
                wifi = self.is_ifn_connected('wlan0')
                if wifi:
                    DeviceLogger.log('info', 'NetworkManager', 'Connection to router established')
                    if self.ping_cloud():  
                        self.stop_modem()
                        self.modem_connected = False
                        self.callback('WiFi')
                    else:
                        DeviceLogger.log('warning', 'NetworkManager', 'No internet through wifi router')
                        # we give up connecting in this case to prevent multiple connection threads 
                        # self.check_or_connect_modem('no internet through wifi router')
                        self.err()
                else:
                    DeviceLogger.log('info', 'NetworkManager', 'No WiFi connection')
                    self.check_or_connect_modem()
        except Exception as ex:
            DeviceLogger.log('error', 'NetworkManager', 'Establish Connection Thread error: ' + str(ex))
            self.err()

    def monitor_cloud_connection(self):
        self.general_monitor_thread = threading.Thread(target=self.monitor, name = 'Cloud Connection Monitor')
        self.general_monitor_thread.start()
        self.running_monitor = True 

    def monitor(self):
        t = threading.currentThread()
        while getattr(t, "running_monitor", True):
            changed = False
            sleep(5) 
            conn = Status.get_device_connection()
            try:
                # if wlan0 is connected and device has other state
                if self.is_ifn_connected('wlan0') and self.ping_cloud():  
                    if self.pool_timer != None and conn != 1 and conn != 5 and conn != 7 :
                        DeviceLogger.log('info', 'NetworkManager', 'Found WiFi')
                        self.check_connection_status()
                    else:
                        DeviceController.set_device_connection(9)
                # if ppp0 is connected 
                elif self.is_ifn_connected('ppp0'):
                    DeviceController.set_device_connection(10)
                    # TO DO: Decide if a GPRS connection can be set outside the device app
                    # if self.pool_timer != None and conn != 2 and conn != 6 and conn != 8 :
                    #     DeviceLogger.log('info', 'NetworkManager', 'Found GPRS')
                    #     self.pool_timer.cancel()
                    #     self.modem_connected = True
                    #     self.callback('GPRS')
                else:
                    DeviceController.set_device_connection(8)
            except Exception as ex:
                DeviceLogger.log('error', 'NetworkManager', 'Cloud Connection Monitor exception: ' + str(ex))


    def check_connection_status(self):
        if self.time != None:
            if self.pool_timer.isAlive():
                self.pool_timer.cancel()
                self.start_timer(3)
            else:
                DeviceLogger.log('info', 'NetworkManager', 'Waiting for Establish Connection Thread to finish')

    def start_timer(self, custom_time = None):
        # timer for counting time until the next connection
        if custom_time != None:
            self.timer = custom_time
        self.pool_timer = threading.Timer(self.timer, self.start)
        self.time = time.time()
        self.pool_timer.name = 'Pool Timer'
        DeviceLogger.log('info', 'NetworkManager', 'Starting pool timer: {0} sec'.format(self.timer))
        self.pool_timer.start()

    def get_pool_timer_alive(self):
        if self.pool_timer == None:
            return False
        else:
            return self.pool_timer.is_alive()

    def cancel_timer(self):
        self.pool_timer.cancel()

    def is_ifn_connected(self, ifn):
        # check if interface ifn interface is connected
        try:
            p = Popen(['ip route list'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            connected,err = p.communicate()  
            output = str(connected).split()
            if ifn in output:
                return True
            else:
                return False
        except Exception as ex:
            DeviceLogger.log('error', 'NetworkManager', 'Cannot check wifi connection: ' + str(ex))
            return False

    def check_or_connect_modem(self, case = 'no wifi'):
        if self.modem_connected:
            DeviceLogger.log('info', 'NetworkManager', 'GPRS connection active')
            if self.ping_cloud():
                self.callback('GPRS')
            else:
                self.stop_modem()
        elif not self.modem_connected and Status.get_modem_status():
            DeviceLogger.log('info', 'NetworkManager', 'Trying GPRS connection')
            self.start_customprovider(case)
        else:
            self.err()

    def start_customprovider(self, case = 'no wifi' ):
        # start modem
        # stop all pppd connections first, to avoid multile instances
        self.stop_modem()
        # try to start pppd connection
        sleep(4)
        try:
            p = Popen(['sudo pon customprovider'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            connected,err = p.communicate() # = p.stdout.readline()
            if err:
                DeviceLogger.log('error', 'NetworkManager', 'Error trying to start pppd: ' + str(err))
            else:
                DeviceLogger.log('info', 'NetworkManager', 'PPPD started')
            sleep(12)
        except Exception as ex:
            DeviceLogger.log('error', 'NetworkManager', 'Exception trying to start pppd: ' + str(ex))
        
        for i in range(3):
            if not self.modem_connected:
                if self.is_ifn_connected('ppp0'):
                    DeviceLogger.log('info', 'NetworkManager', 'Connected to PPPD')
                    self.modem_connected = True
                    if case == 'no internet through wifi router':
                        p = Popen(['sudo route add default dev ppp0'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                        connected,err = p.communicate()
                        sleep(3)
                    self.callback('GPRS')
                    return
                else:
                    sleep(10)

        if not self.modem_connected:
            DeviceLogger.log('info', 'NetworkManager', 'Cannot connect to cell')
            self.err()

 
    def ping_cloud(self):
        # check cloud response
        mem1 = 0
        for i in range(3):
            try:
                host = socket.gethostbyname("cloud.tokinomo.com") #Change to personal choice of site
                s = socket.create_connection((host, 443), 2)
                s.close()
                mem2 = 1
                if (mem2 == mem1):
                    pass #Add commands to be executed on every check
                else:
                    mem1 = mem2
                    return True
            except Exception as e:
                mem2 = 0
                if (mem2 == mem1):
                    pass
                else:
                    mem1 = mem2
                    return False
            sleep(3) #timeInterval for checking
        return False
 
    def stop_modem(self):
        # stop modem pppd
        self.modem_connected = False
        try:
            p = Popen(['sudo poff -a'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            connected,err = p.communicate() # = p.stdout.readline()
            if err:
                DeviceLogger.log('error', 'NetworkManager', 'Error trying to stop pppd: ' + str(err))
            else:
                DeviceLogger.log('info', 'NetworkManager', 'PPPD stopped')
        except Exception as ex:
            DeviceLogger.log('error', 'NetworkManager', 'Exception trying to stop pppd: ' + str(ex))

    def set_timer(self, value):
        self.timer = value