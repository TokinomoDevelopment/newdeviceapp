import threading
from subprocess import Popen, PIPE 
from time import sleep
import gc 
import sys

from ble.bluetoothserver import BleServer
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger
from status import Status


class BleManager(object):

    def __init__(self):
        # delete all recorded mac addresses info and restart service
        # used to reset characteristics for Master, Slave sometimes doesn't find all characteristics without this resest
        proc = Popen(['sudo rm -r /var/lib/bluetooth/* && sudo service bluetooth restart && echo "power on" | sudo bluetoothctl' ],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
        out, err = proc.communicate()  
        sleep(3)
        self.server = None
        BluetoothLogger.log('info', 'Blemanager', 'Started bluetooth manager') 
        DeviceLogger.log('info', 'Blemanager', 'Started bluetooth manager') 

    def reset_blemanager(self):
        self.server.network_down = True
        sleep(0.1)
        BluetoothLogger.log('info', 'Blemanager', 'RESET bluetooth server') 
        DeviceLogger.log('info', 'Blemanager', 'RESET bluetooth server') 
        proc = Popen(['sudo rm -r /var/lib/bluetooth/* && sudo service bluetooth restart && echo "power on" | sudo bluetoothctl' ],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
        out, err = proc.communicate()  
        sleep(2)
        # remove Emit bindings
        Status.reset_bind()
        self.server.bleno.off_all()
        self.server.bleno._bindings._hci.off_all()
        self.server.bleno._bindings._gap.off_all()
        self.server.bleno._bindings._gatt.off_all()
        self.server.bleno._bindings.stopAdvertising()
        # remove attributes
        for k in list(self.server.__dict__.keys()):
            delattr(self.server, k) 
            gc.collect()
        for k in list(self.__dict__.keys()):
            delattr(self, k) 
            gc.collect()
        # restart server
        self.server = None
        self.start_server()
 
    def start_server(self):
        try:
            if self.server == None:
                self.server = BleServer(self.on_mobile_connected, self.on_mobile_disconnected, self.on_command_received, self.reset_blemanager)
                self.server.start()
        except Exception as ex:
            BluetoothLogger.log('error', 'Blemanager', 'Cannot start bluetooth server: ' + str(ex))
            DeviceLogger.log('error', 'Blemanager', 'Cannot start bluetooth server: ' + str(ex))

    def restartBluetooth(self):
        self.server.restartBluetooth()

    # the following 3 functions are needed for piping but are dummy functions

    def on_mobile_connected(self):
        pass

    def on_mobile_disconnected(self):
        pass
        
    def on_command_received(self):
        pass
 