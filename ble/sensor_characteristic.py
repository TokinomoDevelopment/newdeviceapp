import array
import struct
import sys
import traceback
from timeit import default_timer as timer


from time import sleep
import threading

from pybleno import Characteristic
from device.controller import DeviceController
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger
from status import Status

class SensorCharacteristic(Characteristic):
    
    def __init__(self, uuid,reset, pipe):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['read', 'write', 'notify'],
            'value': None
          })
        self.reset = reset
        self._value = array.array('B', [0] * 0)
        self._updateValueCallback = None
        self.pipe = pipe
        self.uuid = uuid
        self.monitor_sensor_thread = None
          
    def onReadRequest(self, offset, callback):
        if(callback):
            callback(Characteristic.RESULT_SUCCESS, self._value)

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        try:
            self.reset()
            command_string = "".join([self.convert_hex_to_ascii(c) for c in data])  
            args = command_string.rstrip().split(',')
            command = int(args[0])
            if command == 2:
                if args[1] == 'light':
                    # toggle light
                    if int(args[2]) == 1:
                        DeviceController.toggle_light(1)
                        BluetoothLogger.log('info', 'SensorChar', 'Device light on')
                        DeviceLogger.log('info', 'SensorChar', 'Device light on')
                    elif int(args[2]) == 0:
                        DeviceController.toggle_light(0)
                        BluetoothLogger.log('info', 'SensorChar', 'Device light off')
                        DeviceLogger.log('info', 'SensorChar', 'Device light off')
                elif args[1] == 'fan':
                    # toggle fan
                    if int(args[2]) == 1:
                        DeviceController.test_fan(1)
                        BluetoothLogger.log('info', 'SensorChar', 'Fan state set to 1 and 255 speed')
                        DeviceLogger.log('info', 'SensorChar', 'Fan state set to 1 and 255 speed')
                    elif int(args[2]) == 0:
                        DeviceController.test_fan(0)
                        BluetoothLogger.log('info', 'SensorChar', 'Fan state set to 0 and 0 speed')
                        DeviceLogger.log('info', 'SensorChar', 'Fan state set to 0 and 0 speed')
            elif command == 3:
                # get date
                rtc = DeviceController.get_datetime()
                date_list = rtc.split(':')
                rtc_string = ''
                for d in date_list:
                    if len(d) == 1:
                        d = '0' + d 
                    rtc_string += d + ':'
                rtc_string = rtc_string[:-1]
                if self._updateValueCallback:
                    enddata = bytearray(rtc_string.encode())
                    self._updateValueCallback(enddata)
                    BluetoothLogger.log('info', 'SensorChar', 'Time {0} sent to mobile app'.format(rtc_string))
                    DeviceLogger.log('info', 'SensorChar', 'Time {0} sent to mobile app'.format(rtc_string))
            elif command == 5:
                if args[1] == 'read':
                    BluetoothLogger.log('info', 'SensorChar', 'Checking sensor')
                    DeviceLogger.log('info', 'SensorChar', 'Checking sensor')
                    DeviceController.set_sensor_checker(True)
                    self.monitor_sensor()
                if args[1] == 'stop':
                    BluetoothLogger.log('info', 'SensorChar', 'Stop command received')
                    DeviceLogger.log('info', 'SensorChar', 'Stop command received')
                    DeviceController.set_sensor_checker(False)
            elif command == 20:
                temp = Status.get_amb_temp()
                if self._updateValueCallback:
                    enddata = bytearray(str(temp).encode())
                    BluetoothLogger.log('info', 'SensorChar', 'Temperature {0} degrees sent to mobile app'.format(temp))
                    DeviceLogger.log('info', 'SensorChar', 'Temperature {0} degrees sent to mobile app'.format(temp))
                    self._updateValueCallback(enddata)
            if(callback):
                callback(Characteristic.RESULT_SUCCESS)
        except Exception as ex:
            BluetoothLogger.log('error', 'MotionChar', 'Error on write request: ' + str(ex))
            DeviceLogger.log('error', 'MotionChar', 'Error on write request: ' + str(ex))
    
    def sensor_received(self):
        # DeviceController.set_sensor_checker(False)
        if self._updateValueCallback:
            BluetoothLogger.log('info', 'SensorChar', 'Sensor OK')
            DeviceLogger.log('info', 'SensorChar', 'Sensor OK')
            enddata = bytearray(b'1')
            self._updateValueCallback(enddata)
        
    def onSubscribe(self, maxValueSize, updateValueCallback):
        self._updateValueCallback = updateValueCallback

    def onUnsubscribe(self):
        self._updateValueCallback = None
    
    def convert_hex_to_ascii(self,h):
        chars_in_reverse = []
        while h != 0x0:
            chars_in_reverse.append(chr(h & 0xFF))
            h = h >> 8
        chars_in_reverse.reverse()
        return ''.join(chars_in_reverse)

    def monitor_sensor(self):
        self.monitor_sensor_thread = threading.Thread(target=self.sensor_monitor, name = 'Mobile app sensor monitor')
        self.monitor_sensor_thread.start()

    def sensor_monitor(self):
        t = threading.currentThread()
        while DeviceController.get_sensor_checker():
            sleep(0.1)
            if DeviceController.get_sensor_status():
                self.sensor_received()
                DeviceController.set_sensor_status(False)