import array

from pybleno import Characteristic
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger

class QuitCharacteristic(Characteristic):
    
    def __init__(self, uuid, cb, reset, pipe):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['read', 'write', 'notify'],
            'value': None
          })
 
        self.cb = cb  
        self._updateValueCallback = None
        self.pipe = pipe
        self.uuid = uuid
          
    def onReadRequest(self, offset, callback):
        callback(Characteristic.RESULT_SUCCESS, self._value[offset:])

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        try:
            cmd_string = "".join([self.convert_hex_to_ascii(c) for c in data])  
            args = cmd_string.rstrip().split(',')
            command = int(args[0])
            if command == 99:
                self.cb()
            elif command == 100:
                self.cb()
            callback(Characteristic.RESULT_SUCCESS)
        except Exception as ex:
            BluetoothLogger.log('error', 'QuitChar', 'Error on write request: ' + str(ex))
            DeviceLogger.log('error', 'QuitChar', 'Error on write request: ' + str(ex))
    
    def onSubscribe(self, maxValueSize, updateValueCallback):
        self._updateValueCallback = updateValueCallback

    def onUnsubscribe(self):
        self._updateValueCallback = None


    def convert_hex_to_ascii(self,h):
        chars_in_reverse = []
        while h != 0x0:
            chars_in_reverse.append(chr(h & 0xFF))
            h = h >> 8
        chars_in_reverse.reverse()
        return ''.join(chars_in_reverse)
        