from pybleno import Characteristic
import array
import threading
from time import sleep
 
from device.controller import DeviceController
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger
from status import Status

class DialogCharacteristic(Characteristic):
    
    def __init__(self, uuid, disconnect, restartBluetooth, reset):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['write','read','notify','writeWithoutResponse'],
            'value': None
          })
        self._value = array.array('B', [0] * 0)
        self._updateValueCallback = None
        self.disconnect = disconnect
        self.uuid = uuid
        self.restartBluetooth = restartBluetooth
        self.dialogue_alive_timer = None
        self.reset = reset
        if Status.get_setting('DIALOG_ROLE') == 'MASTER':
            DeviceController.reset_bind()
            DeviceController.bind_to(self.master_slave_wrapper)
            self.control_from_slave = False
            self.monitor_blt_connection_from_master()

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        cmd_string = "".join([self.convert_hex_to_ascii(c) for c in data])  
        BluetoothLogger.log('info', 'DialogChar', 'DIALOGUE: Master received ' + cmd_string)
        DeviceLogger.log('info', 'DialogChar', 'DIALOGUE: Master received ' + cmd_string)
        args = cmd_string.rstrip().split(',')
        command = int(args[0])
        if command == 1:
            # connection established
            DeviceController.set_dialogue_index(0)
            BluetoothLogger.log('info', 'BluetoothServer', 'DIALOGUE: Connection established from Master')
            DeviceLogger.log('info', 'BluetoothServer', 'DIALOGUE: Connection established from Master')
            if args[1] == 'init':
                if not Status.check_standby_status():
                    Status.set_device_on(True)
                Status.set_dialogue_connected(True)
                # restart Master -> Slave control message thread if connection is established
        elif command == 2:
            # activate device if Master
            if int(args[1]) == 0:
            # if Master receives index 0 Slave played last file so we reset index:
                DeviceController.set_dialogue_index(0)
                Status.select_dialogue_list()
            else:
            # play file received from Slave
                DeviceController.set_dialogue_index(int(args[1]))
                DeviceController.activate_device()
        elif command == 3:
            self.control_from_slave = True
        callback(Characteristic.RESULT_SUCCESS)

    def onWriteRequestFromDeviceController(self, msg):
        # Master -> Slave 
        try:
            self._updateValueCallback(bytearray(str(msg).encode()))
            BluetoothLogger.log('info', 'DialogChar', 'DIALOGUE: Master sent ' + str(msg))
            DeviceLogger.log('info', 'DialogChar', 'DIALOGUE: Master sent ' + str(msg))
        except Exception as ex:
            BluetoothLogger.log('error', 'Controller', 'DIALOGUE: Slave no longer connected: ' + str(ex))
            DeviceLogger.log('error', 'Controller', 'DIALOGUE: Slave no longer connected: ' + str(ex))
            self.disconnect()

    def convert_hex_to_ascii(self,h):
        chars_in_reverse = []
        while h != 0x0:
            chars_in_reverse.append(chr(h & 0xFF))
            h = h >> 8
        chars_in_reverse.reverse()
        return ''.join(chars_in_reverse)
    
    def onSubscribe(self, maxValueSize, updateValueCallback):
        self._updateValueCallback = updateValueCallback

    def onUnsubscribe(self):
        self._updateValueCallback = None
 
    def master_slave_wrapper(self):
        # Master -> Slave wrapper 
        self.reset(time_value = 120)
        if Status.get_setting('DIALOG_ROLE') == 'MASTER':
            DeviceController.set_send_dialog_msg(False) 
            DeviceController.set_dialogue_index(DeviceController.get_dialogue_index() + 1)
            # if Master plays the last file reset index
            if DeviceController.get_dialogue_index() == len(Status.get_dialogue_list()[Status.get_list_index()]):
                DeviceController.set_dialogue_index(0)
                Status.select_dialogue_list()
                # restart Master -> Slave control message thread if dialogue is finished
                DeviceController.release_running_blt_master_monitor()
            # else send index to be played to Slave
            else:
                self.onWriteRequestFromDeviceController('2,{0},{1}'.format(DeviceController.get_dialogue_index(),Status.get_list_index()))

    def monitor_blt_connection_from_master(self):
        self.blt_monitor_thread = threading.Thread(target=self.blt_connection_monitor_from_master, name = 'blt Connection Monitor')
        self.blt_monitor_thread.start()
        self.running_blt_monitor = True 

    def blt_connection_monitor_from_master(self):
        sleep(3)
        t = threading.currentThread()
        while True:
            self.control_from_slave = False
            if Status.get_dialogue_connected() and DeviceController.get_dialogue_index() == 0 and not DeviceController.get_is_playing():
                # Master sends message and expects return from Slave
                self.onWriteRequestFromDeviceController('3,control')
                sleep(1)
                if not self.control_from_slave:
                    DeviceLogger.log('error', 'Controller', 'DIALOGUE: Control not received from Slave')
                    self.disconnect()
                else:
                    self.reset(time_value = 600)
            sleep(59) 
 