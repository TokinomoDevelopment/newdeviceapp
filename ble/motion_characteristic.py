import array
import struct
import sys
import threading

from device.controller import DeviceController
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger
from pybleno import Characteristic

class MotionCharacteristic(Characteristic):
    
    def __init__(self, uuid, reset, pipe):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['write','writeWithoutResponse'],
            'value': None
          })
        self.reset = reset
        self._value = array.array('B', [0] * 0)
        self._updateValueCallback = None
        self.pipe = pipe
        self.uuid = uuid

    def onReadRequest(self, offset, callback):
        if(callback):
            callback(Characteristic.RESULT_SUCCESS, self._value)

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        try:
            self.reset()
            command_string = "".join([self.convert_hex_to_ascii(c) for c in data])  
            args = command_string.rstrip().split(',')
            command = int(args[0])
            if command == 1:
                if args[1] == 'extend':
                    BluetoothLogger.log('info', 'MotionChar', 'Extend command called')
                    DeviceLogger.log('info', 'MotionChar', 'Extend command called')
                    DeviceController.move_home()
                    BluetoothLogger.log('info', 'MotionChar', 'Device activated: extend')
                    DeviceLogger.log('info', 'MotionChar', 'Device activated: extend')
                    DeviceController.extend()
                elif args[1] == 'home':
                    BluetoothLogger.log('info', 'MotionChar', 'Home command called')
                    DeviceLogger.log('info', 'MotionChar', 'Home command called')
                    DeviceController.move_home()
                elif args[1] == 'bounce':
                    BluetoothLogger.log('info', 'MotionChar', 'Bounce command called')
                    DeviceLogger.log('info', 'MotionChar', 'Bounce command called')
                    DeviceController.move_home()
                    DeviceController.extend_bounce()
            if(callback):
                callback(Characteristic.RESULT_SUCCESS)
        except Exception as ex:
            BluetoothLogger.log('error', 'MotionChar', 'Error on write request: ' + str(ex))
            DeviceLogger.log('error', 'MotionChar', 'Error on write request: ' + str(ex))

    def onSubscribe(self, maxValueSize, updateValueCallback):
        self._updateValueCallback = updateValueCallback

    def onUnsubscribe(self):
        self._updateValueCallback = None

    
    def convert_hex_to_ascii(self,h):
        chars_in_reverse = []
        while h != 0x0:
            chars_in_reverse.append(chr(h & 0xFF))
            h = h >> 8
        chars_in_reverse.reverse()
        return ''.join(chars_in_reverse)