import array
from subprocess import Popen, PIPE
import re
from time import sleep
import threading
import json
import os
from datetime import datetime 
from dateutil import tz as tzz

from pybleno import Characteristic
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger
from device.controller import DeviceController
from status import Status
from device.board import Board

class ConnectionsCharacteristic(Characteristic):
    
    def __init__(self, uuid, cb, reset, on_command_received):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['read', 'write', 'notify'],
            'value': None
          })
 
        self.reset = reset
        self._value = array.array('B', [0] * 0)
        self._updateValueCallback = None
        self.networks = []
        self.cb = cb
        self.on_command_received = on_command_received
        self.uuid = uuid
        self.dev_path = '/home/pi/device/device.txt'
        self.apn_timestamp = '/home/pi/device/apn_timestamp.txt'
          
    def onReadRequest(self, offset, callback):
        callback(Characteristic.RESULT_SUCCESS, self._value[offset:])

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        DeviceController.stop_device()
        self.reset()
        try:
            command_string = "".join([self.convert_hex_to_ascii(c) for c in data])
            args = command_string.rstrip().split(',')
            command = int(args[0])
            if command == 109: 
                # command to send available networks to mobile app
                BluetoothLogger.log('info', 'ConnectionChar', 'Command for scanning wifi networks sent')
                DeviceLogger.log('info', 'ConnectionChar', 'Command for scanning wifi networks sent')
                if self._updateValueCallback:
                    try:
                        for net in self.scan():
                            value = net.get('ssid')
                            response = bytearray(value.encode()) 
                            self._updateValueCallback(response)
                            BluetoothLogger.log('info', 'ConnectionChar', 'Network sent to mobile app: {0}'.format(str(value)))
                            DeviceLogger.log('info', 'ConnectionChar', 'Network sent to mobile app: {0}'.format(str(value)))
                            sleep(0.1)
                        enddata = bytearray(b'enddata')
                        self._updateValueCallback(enddata)
                        if(callback):
                            callback(Characteristic.RESULT_SUCCESS)
                    except Exception as ex:
                        BluetoothLogger.log('error', 'ConnectionChar', 'Cannot send networks to mobile app')
                        DeviceLogger.log('error', 'ConnectionChar', 'Cannot send networks to mobile app')
                        self._updateValueCallback(array.array('B', [0] * 0))
            elif command == 30:
                # command to set available network 
                ssid = args[1]
                password = args[2]
                if ssid:
                    try:
                        s = Popen(['sudo nmcli c delete {0}'.format(ssid)],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                        deleted,err = s.communicate()
                        if err:
                            BluetoothLogger.log('info', 'ConnectionChar', '{0} network not registered'.format(ssid))
                            DeviceLogger.log('info', 'ConnectionChar', '{0} network not registered'.format(ssid))
                        else:
                            BluetoothLogger.log('info', 'ConnectionChar', '{0} network deleted'.format(ssid))
                            DeviceLogger.log('info', 'ConnectionChar', '{0} network deleted'.format(ssid))
                            DeviceController.set_device_connection(8)
                        if password == '':
                            p = Popen(['sudo nmcli device wifi connect "{0}"'.format(ssid)],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                        else:
                            p = Popen(['sudo nmcli device wifi connect "{0}" password {1}'.format(ssid,password)],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                        connected,err = p.communicate()
                        if err:
                            BluetoothLogger.log('info', 'ConnectionChar', 'Error seting {0} network'.format(ssid))
                            DeviceLogger.log('info', 'ConnectionChar', 'Error seting {0} network'.format(ssid))
                            if(callback):
                                callback(Characteristic.RESULT_UNLIKELY_ERROR)
                        else:
                            if self.is_interface_connected('wlan0'):
                                BluetoothLogger.log('info', 'ConnectionChar', 'Connection established!')
                                DeviceLogger.log('info', 'ConnectionChar', 'Connection established!')
                                if(callback):
                                    callback(Characteristic.RESULT_SUCCESS)   
                            else:
                                BluetoothLogger.log('error', 'ConnectionChar', 'Connection not established! Please check wifi password!')
                                DeviceLogger.log('error', 'ConnectionChar', 'Connection not established! Please check wifi password!')
                                if(callback):
                                    callback(Characteristic.RESULT_UNLIKELY_ERROR)
                    except:
                        BluetoothLogger.log('error', 'ConnectionChar', 'Error connecting')
                        DeviceLogger.log('error', 'ConnectionChar', 'Error connecting')
                        if(callback):
                            callback(Characteristic.RESULT_UNLIKELY_ERROR)
                else:
                    if(callback):
                        callback(Characteristic.RESULT_UNLIKELY_ERROR)
            elif command == 120:
                # command to set GSM network
                if args[2] == '' or args[3] == '':
                    Popen(['sudo','/home/pi/device/modemconn/createcustomppp',args[1]]).wait()
                    # set iot provider in device.txt
                    config = None
                    try:
                        with open(self.dev_path, "r") as f:
                            config = f.read()
                            config = json.loads(config)
                            config['iot_provider'] = args[1]
                        Status.set_iot_provider(args[1])
                    except Exception as ex:
                        BluetoothLogger.log('error', 'ConnectionChar', 'Cannot load device file' + str(ex))
                        DeviceLogger.log('error', 'ConnectionChar', 'Cannot load device file' + str(ex))
                    try:
                        Status.save_device_file(config)
                        BluetoothLogger.log('info', 'ConnectionChar', 'Wrote new iot provider in device file')
                        DeviceLogger.log('info', 'ConnectionChar', 'Wrote new iot provider in device file')
                        utc_offset = datetime.now(tzz.gettz(Status.get_timezone())).utcoffset()  
                        board_unixtime = ((Board.read_rtc_date() - datetime(1970, 1, 1)).total_seconds() - utc_offset.total_seconds())*1000
                        Status.set_apn_timestamp(board_unixtime)
                    except Exception as ex:
                        BluetoothLogger.log('error', 'ConnectionChar', 'Cannot update iot provider in device file: ' + str(ex))
                        DeviceLogger.log('error', 'ConnectionChar', 'Cannot update iot provider in device file: ' + str(ex))
                    # modify custom provider with new provider apn
                    try:
                        cmd = 'sudo sed -i ' + "'s/-T .*/-T {0}".format(args[1]) + '"/' + "' /etc/ppp/peers/customprovider"
                        p = Popen([cmd], stdout=PIPE, shell=True)
                        DeviceLogger.log('info', 'Status', 'Wrote {0} to customprovider file'.format(args[1]))
                        BluetoothLogger.log('info', 'Status', 'Wrote {0} to customprovider file'.format(args[1]))
                    except Exception as ex:
                        DeviceLogger.log('error', 'Status', 'Cannot write {0} to customprovider file : {1}'.format(args[1], str(ex)))
                        BluetoothLogger.log('error', 'Status', 'Cannot write {0} to customprovider file : {1}'.format(args[1], str(ex)))
                else:
                    Popen(['sudo','/home/pi/device/modemconn/createcustomppp',args[1],'auth',args[2],args[3]]).wait()
                    BluetoothLogger.log('info', 'ConnectionChar', 'Iot provider set to: {0}, username set to: {1}, password set to: {2}'.format(args[1],args[2],args[3]))
                    DeviceLogger.log('info', 'ConnectionChar', 'Iot provider set to: {0}, username set to: {1}, password set to: {2}'.format(args[1],args[2],args[3]))
                    utc_offset = datetime.now(tzz.gettz(Status.get_timezone())).utcoffset()  
                    board_unixtime = ((Board.read_rtc_date() - datetime(1970, 1, 1)).total_seconds() - utc_offset.total_seconds())*1000
                    Status.set_apn_timestamp(board_unixtime)
                    Status.set_iot_provider(args[1])
                if(callback):
                    callback(Characteristic.RESULT_SUCCESS)
            elif command == 199:
                # send GSM signal to mobile app
                signal = Status.get_modem_signal()
                if signal is None:
                    signal = 'Please refresh'
                response = bytearray(signal.encode())  
                self._updateValueCallback(response)
                BluetoothLogger.log('info', 'ConnectionChar', 'Sent {0} signal to mobile app'.format(signal))
                DeviceLogger.log('info', 'ConnectionChar', 'Sent {0} signal to mobile app'.format(signal))
                if(callback):
                    callback(Characteristic.RESULT_SUCCESS)
            elif command == 200:
                # reboot device
                s = Popen(['sudo reboot'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                reboot,err = s.communicate()
        except Exception as ex:
            BluetoothLogger.log('error', 'ConnectionChar', 'Error on write request: ' + str(ex))
            DeviceLogger.log('error', 'ConnectionChar', 'Error on write request: ' + str(ex))

    def onSubscribe(self, maxValueSize, updateValueCallback):
        self._updateValueCallback = updateValueCallback

    def onUnsubscribe(self):
        self._updateValueCallback = None

    def scan( self, callback=None):
        p = Popen(['nmcli -f SSID dev wifi'],stdin=PIPE, stdout=PIPE, shell=True)
        networks, err = p.communicate()
        if err and callback:
            BluetoothLogger.log('error', 'ConnectionChar', 'Error scanning for wifi networks: ' + str(err))
            DeviceLogger.log('error', 'ConnectionChar', 'Error scanning for wifi networks: ' + str(err))
            callback(stderr)
            return
        networks = networks.decode()
        lines = networks.splitlines()
        ret_networks = []
        for line in lines[1:]:
            ret_networks.append({'ssid' : line.rstrip()})
            BluetoothLogger.log('info', 'ConnectionChar', 'Detected network: ' + str(line.rstrip()))
            DeviceLogger.log('info', 'ConnectionChar', 'Detected network: ' + str(line.rstrip()))
        return ret_networks  

    def convert_hex_to_ascii(self,h):
        chars_in_reverse = []
        while h != 0x0:
            chars_in_reverse.append(chr(h & 0xFF))
            h = h >> 8
        chars_in_reverse.reverse()
        return ''.join(chars_in_reverse)

    def is_interface_connected(self, ifn):
        # check if ifn interface is connected
        connected = 0
        try:
            p = Popen(['cat /sys/class/net/{0}/carrier'.format(ifn)],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            connected,err = p.communicate()  
            if err:
                return False
            connected = int(connected.decode('utf-8'))
        except:
            return False

        if connected == 1:
            return True
        else:
            return False 