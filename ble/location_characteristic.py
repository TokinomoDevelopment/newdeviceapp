import array
import struct
import sys
import traceback

from status import Status
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger
from pybleno import Characteristic

class LocationCharacteristic(Characteristic):
    
    def __init__(self, uuid, reset, pipe):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['write'],
            'value': None
          })
        self.reset = reset
        self._value = array.array('B', [0] * 0)
        self._updateValueCallback = None
        self.pipe = pipe
        self.uuid = uuid

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        try:
            self.reset()
            cmd_string = "".join([self.convert_hex_to_ascii(c) for c in data])  
            args = cmd_string.rstrip().split(',')
            command = int(args[0])
            if command == 40:
                location = [float(args[1]), float(args[2])]
                Status.set_device_location_app(location)
            callback(Characteristic.RESULT_SUCCESS)
        except Exception as ex:
            BluetoothLogger.log('error', 'LocationChar', 'Error on write request: ' + str(ex))        
            DeviceLogger.log('error', 'LocationChar', 'Error on write request: ' + str(ex))        
    
    def convert_hex_to_ascii(self,h):
        chars_in_reverse = []
        while h != 0x0:
            chars_in_reverse.append(chr(h & 0xFF))
            h = h >> 8

        chars_in_reverse.reverse()
        return ''.join(chars_in_reverse)