from ble.bleno import *
import sys
from threading import Timer
import threading
from time import sleep
from subprocess import Popen, PIPE 
import os, shutil

from status import Status
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger
from ble.connections_characteristic import ConnectionsCharacteristic
from ble.quit_characteristic import QuitCharacteristic
from ble.motion_characteristic import MotionCharacteristic
from ble.sensor_characteristic import SensorCharacteristic
from ble.location_characteristic import LocationCharacteristic
from ble.audio_characteristic import AudioCharacteristic
from ble.dialog_characteristic import DialogCharacteristic
from device.controller import DeviceController
from ble.gatt.gatt_linux import DeviceManager
from ble.gatt.gatt_linux import Device



class DialogueDeviceManager(DeviceManager):

    def __init__(self, receive_data_handler_cb, monitor_blt_connection_from_slave, adapter_name):
        super().__init__(adapter_name)
        self.dialog_characteristic = None
        self.monitor_blt_connection_from_slave = monitor_blt_connection_from_slave
        self.receive_data_handler_cb = receive_data_handler_cb

    def device_discovered(self, device):
        if device.alias() == 'Tokinomo_Device{0}-{1}'.format(Status.get_setting('MASTER_ID'),Status.get_campaign_id()):
            self.stop_discovery()
            self.device = DialogueDevice(device.mac_address, self)
            self.device.connect()
            if self.dialog_characteristic != None:
                self.dialog_characteristic.enable_notifications()


class DialogueDevice(Device):

    def __init__(self, mac_address, manager):
        super().__init__( mac_address, manager, managed = True)
        self.manager = manager

    def connect_succeeded(self):
        super().connect_succeeded()

    def connect_failed(self, error):
        super().connect_failed(error)
        BluetoothLogger.log('error', 'BluetoothServer','[%s] Connection failed: %s' % (self.mac_address, str(error)))
        DeviceLogger.log('error', 'BluetoothServer','[%s] Connection failed: %s' % (self.mac_address, str(error)))
        Status.set_dialogue_connected(False)
        Status.set_scanning(True)

    def disconnect_succeeded(self):
        super().disconnect_succeeded()
        Status.set_dialogue_connected(False)
        BluetoothLogger.log('error', 'BluetoothServer','DIALOGUE: Master no longer connected')
        DeviceLogger.log('error', 'BluetoothServer','DIALOGUE: Master no longer connected')
        Status.set_scanning(True)

    def services_resolved(self):
        super().services_resolved()
        for service in self.services:
            for characteristic in service.characteristics:
                if characteristic.uuid == "0000ec6f-0000-1000-8000-00805f9b34fb":
                    characteristic.write_value(bytearray(bytes('1,init', encoding='utf8')))
                    self.manager.dialog_characteristic = characteristic
                    BluetoothLogger.log('info', 'BluetoothServer', 'DIALOGUE: Connection established from Slave')
                    DeviceLogger.log('info', 'BluetoothServer', 'DIALOGUE: Connection established from Slave')
                    Status.set_dialogue_connected(True)
                    self.manager.monitor_blt_connection_from_slave()
    def characteristic_value_updated(self, characteristic, value):
        self.manager.receive_data_handler_cb(value)
 

class BleServer():

    def __init__(self, on_mobile_connected, on_mobile_disconnected, on_command_received, reset_blemanager):

        self.bleno = Bleno(reset_blemanager)
        self.bleno.on('stateChange', self.onStateChange)
        self.bleno.on('advertisingStart', self.onAdvertisingStart)
        self.bleno.on('accept',self.onClientConnected)
        self.bleno.on('disconnect', self.disconnect)
        self.server_timer = None
        self.dialogue_alive_timer = None
        self.servicesSet = False
        self.clients = []
        self.device = None
        self.on_mobile_connected = on_mobile_connected
        self.on_mobile_disconnected = on_mobile_disconnected
        self.on_command_received = on_command_received
        self.advertising = False
        self.network_down = False
        self.manager = DialogueDeviceManager(self.receive_data_handler_cb, self.monitor_blt_connection_from_slave, adapter_name='hci0') 
        self.dialog_characteristic = None
        Status.set_dialogue_connected(False)
        Status.bind_to(self.scan)
        BluetoothLogger.log('info', 'BluetoothServer', 'Ble server initialised')
        DeviceLogger.log('info', 'BluetoothServer', 'Ble server initialised')
 
    def connect(self):
        pass

    def start(self, cb=None):
        self.bleno.start()
        BluetoothLogger.log('info', 'BluetoothServer', 'Started bluetooth server')
        DeviceLogger.log('info', 'BluetoothServer', 'Started bluetooth server')

    def onStateChange(self, state):
        if (state == 'poweredOn'):  
            self.start_advertising_wrapper()
        else:
            BluetoothLogger.log('info', 'BluetoothServer', 'Bluetooth state is ' + str(state))
            DeviceLogger.log('info', 'BluetoothServer', 'Bluetooth state is ' + str(state))

    def onAdvertisingStart(self, error = False):
        if not self.advertising:
            BluetoothLogger.log('info', 'BluetoothServer', 'Advertising Device services')
            DeviceLogger.log('info', 'BluetoothServer', 'Advertising Device services')
            self.bleno.setServices([ 
                    BlenoPrimaryService({
                        'uuid': 'ec00',
                        'characteristics': [ 
                                MotionCharacteristic('ec0f',self.reset_timer, self.on_command_received),
                                ConnectionsCharacteristic('ec1f',self.connect,self.reset_timer, self.on_command_received),
                                QuitCharacteristic('ec2f',self.disconnect,self.reset_timer, self.on_command_received),
                                SensorCharacteristic('ec3f',self.reset_timer, self.on_command_received),
                                LocationCharacteristic('ec4f',self.reset_timer, self.on_command_received),
                                AudioCharacteristic('ec5f',self.reset_timer, self.on_command_received),
                                DialogCharacteristic('ec6f', self.run_when_disconnected, self.restartBluetooth, self.reset_timer)
                            ]
                    })
                ])
            self.advertising = True

    def start_advertising_wrapper(self):
        if Status.get_setting('DIALOG_ROLE') == 'MASTER':
            self.bleno.startAdvertising('Tokinomo_Device' + str(Status.get_device_id()) + '-' + str(Status.get_campaign_id()), ['ec00'])
            BluetoothLogger.log('info', 'BluetoothServer', 'Advertising Device {0} and campaign {1}'.format(str(Status.get_device_id()),str(Status.get_campaign_id())) )
            DeviceLogger.log('info', 'BluetoothServer', 'Advertising Device {0} and campaign {1}'.format(str(Status.get_device_id()),str(Status.get_campaign_id())) )
            Status.set_scanning(False)
            self.manager.stop()
        if Status.get_setting('DIALOG_ROLE') == 'SLAVE':
            DeviceController.reset_bind()
            DeviceController.bind_to(self.slave_master_wrapper)
            Status.set_scanning(True)

    def run_when_disconnected(self):
        try:
            if self.server_timer != None:
                self.server_timer.cancel()
        except Exception as ex:
            BluetoothLogger.log('error', 'BluetoothServer', 'Server timer not initialised: ' + str(ex))
            DeviceLogger.log('error', 'BluetoothServer', 'Server timer not initialised: ' + str(ex))
        self.bleno.disconnect()
        Status.set_mobile_connected(False)
        Status.set_scanning(False)
        DeviceController.set_dialogue_index(0)
        Status.set_dialogue_connected(False)
        if Status.get_setting('USE_DIALOG'):
            Status.set_device_on(False)
        else:
            Status.set_device_on(True)
            Status.set_dialogue(None)
        # stop possible executing activites
        DeviceController.stop_test_file_play()
        DeviceController.toggle_light(0)
        DeviceController.move_home()
        DeviceController.set_sensor_checker(False)

    def onClientConnected(self,clientAddress):
        Status.set_mobile_connected(True)
        DeviceController.move_home()
        BluetoothLogger.log('info', 'BluetoothServer', 'Home command called')
        DeviceLogger.log('info', 'BluetoothServer', 'Home command called')
        DeviceController.stop_test_file_play()
        if not clientAddress in self.clients:
            self.clients.append(clientAddress)
        Status.set_device_on(False)       
        BluetoothLogger.log('info', 'BluetoothServer', 'Client {0} connected to device'.format(str(clientAddress)))
        DeviceLogger.log('info', 'BluetoothServer', 'Client {0} connected to device'.format(str(clientAddress)))
        Status.set_scanning(False)
        self.reset_timer()

    def disconnectNoActivity(self):
        self.bleno.disconnect()
        BluetoothLogger.log('info', 'BluetoothServer', 'Disconnected due to no activity')    
        DeviceLogger.log('info', 'BluetoothServer', 'Disconnected due to no activity')    

    def disconnect(self,clientAddress):
        self.disconnect_thread = threading.Thread(target=self.execute_disconnect, args=[clientAddress], name = 'Execute disconnect thread')
        self.disconnect_thread.start()

    def execute_disconnect(self, clientAddress):
        # if bluetooth service breaks there's no need to run this part, reset_blemanager takes care of it
        # if bluetooth service breaks we add this sleep for the reset_blemanager to have enough time to delete server object
        if not self.network_down:
            self.run_when_disconnected()
            BluetoothLogger.log('info', 'BluetoothServer', 'Client {0} disconnected from device'.format(str(clientAddress)))
            DeviceLogger.log('info', 'BluetoothServer', 'Client {0} disconnected from device'.format(str(clientAddress)))

    def restartBluetooth(self):
        self.run_when_disconnected()
        BluetoothLogger.log('info', 'BluetoothServer', 'Restarting advertising')
        DeviceLogger.log('info', 'BluetoothServer', 'Restarting advertising')
 
    def reset_timer(self, time_value = 180):
        try:
            if self.server_timer != None:
                self.server_timer.cancel()
        except Exception as ex:
            BluetoothLogger.log('error', 'BluetoothServer', 'Server timer not initialised: ' + str(ex))
            DeviceLogger.log('error', 'BluetoothServer', 'Server timer not initialised: ' + str(ex))
        self.server_timer = Timer(time_value, self.disconnectNoActivity)
        self.server_timer.start()

    def onServicesSet(self, state):
        self.servicesSet = True

    def scan(self):
        # scan for master thread
        self.scan_thread = threading.Thread(target=self.execute_scan, name = 'Scanning device for pairing thread')
        self.scan_thread.start()

    def execute_scan(self):
        # start scanning for master
        if Status.get_scanning():
            try:   
                try:
                    self.manager.remove_all_devices() # this removes info about previously connected Nano Pi devices
                except Exception:
                    pass
                master_id = Status.get_setting('MASTER_ID')
                BluetoothLogger.log('info', 'Blemanager', 'DIALOGUE: Scanning for Master Device {0} in campaign {1} '.format(master_id,Status.get_campaign_id()))
                DeviceLogger.log('info', 'Blemanager', 'DIALOGUE: Scanning for Master Device {0} in campaign {1} '.format(master_id,Status.get_campaign_id()))
                self.manager.start_discovery()
                self.manager.run()
            except Exception as ex:
                BluetoothLogger.log('error', 'Controller', 'DIALOGUE: Error scanning for Master: ' + str(ex))
                DeviceLogger.log('error', 'Controller', 'DIALOGUE: Error scanning for Master: ' + str(ex))
        else:
            self.manager.stop()

    def receive_data_handler_cb(self, data):
        # activate device if Slave
        cmd_string = "".join([self.convert_hex_to_ascii(c) for c in data])
        BluetoothLogger.log('info', 'Blemanager', 'DIALOGUE: Slave received ' + cmd_string)
        DeviceLogger.log('info', 'Blemanager', 'DIALOGUE: Slave received ' + cmd_string)
        args = cmd_string.rstrip().split(',')
        command = int(args[0])
        if command == 2:
            Status.set_list_index(int(args[2]))
            DeviceController.set_dialogue_index(int(args[1]))
            DeviceController.activate_device()
        elif command == 3:
            self.send_msg_to_master('3,control')    
        elif command == 4:
            self.control_from_master = True

    def convert_hex_to_ascii(self,h):
        chars_in_reverse = []
        while h != 0x0:
            chars_in_reverse.append(chr(h & 0xFF))
            h = h >> 8
        chars_in_reverse.reverse()
        return ''.join(chars_in_reverse)

    def send_msg_to_master(self, msg):
        # Slave -> Master
        try:
            self.manager.dialog_characteristic.write_value(bytearray(bytes(msg, encoding='utf8')))
            BluetoothLogger.log('info', 'Blemanager', 'DIALOGUE: Slave sent ' + str(msg))
            DeviceLogger.log('info', 'Blemanager', 'DIALOGUE: Slave sent ' + str(msg))
        except Exception as ex:
            BluetoothLogger.log('error', 'Controller', 'DIALOGUE: Master no longer connected: ' + str(ex))
            DeviceLogger.log('error', 'Controller', 'DIALOGUE: Master no longer connected: ' + str(ex))
            self.run_when_disconnected()
            Status.set_scanning(True)

    def slave_master_wrapper(self):
        # Slave -> Master dialogue wrapper
        self.reset_timer(time_value = 1800)
        if Status.get_setting('DIALOG_ROLE') == 'SLAVE':
            DeviceController.set_send_dialog_msg(False) 
            DeviceController.set_dialogue_index(DeviceController.get_dialogue_index() + 1)
            # if Slave plays the last file send index 0 to Master
            chosen_list = Status.get_dialogue_list()[Status.get_list_index()]
            if DeviceController.get_dialogue_index() == len(chosen_list):
                DeviceController.set_dialogue_index(0)
            self.send_msg_to_master('2,{0}'.format(DeviceController.get_dialogue_index()))
 
    def monitor_blt_connection_from_slave(self):
        self.blt_monitor_thread = threading.Thread(target=self.blt_connection_monitor_from_slave, name = 'blt Connection Monitor')
        self.blt_monitor_thread.start()
        self.running_blt_monitor = True 

    def blt_connection_monitor_from_slave(self):
        # Control message from Slave to Master not implemented, probably not needed
        pass