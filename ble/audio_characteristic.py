import array
import struct
import sys
import traceback
import time
import os

from pybleno import Characteristic
from status import Status
from device.controller import DeviceController
from ble.bluetoothlogger import BluetoothLogger
from devicelogger import DeviceLogger

class AudioCharacteristic(Characteristic):
    
    def __init__(self, uuid, reset, pipe):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['write','notify'],
            'value': None
          })
        self.uuid = uuid
        self.reset = reset 
        self.pipe = pipe
        self._value = array.array('B', [0] * 0)
        self._updateValueCallback = None

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        try:
            self.reset()
            cmd_string = "".join([self.convert_hex_to_ascii(c) for c in data])  
            args = cmd_string.rstrip().split(',')
            command = int(args[0])
            if command == 6:
                self.play(args[1])
            elif command == 7:
                self.stop()
            elif command == 8:
                BluetoothLogger.log('info', 'AudioChar', 'Device volume set to {0}%'.format(args[1]))
                Status.set_device_volume(args[1])
            elif command == 44:
                files = self.list_files()
                if self._updateValueCallback:
                    try:
                        for f in files:
                            response = bytearray(f.encode())
                            self._updateValueCallback(response)
                            BluetoothLogger.log('info', 'AudioChar', 'File name sent to mobile app: {0}'.format(f))
                            DeviceLogger.log('info', 'AudioChar', 'File name sent to mobile app: {0}'.format(f))
                            time.sleep(0.1)
                        vol = Status.get_device_volume()
                        BluetoothLogger.log('info', 'AudioChar', 'Volume sent to mobile app: {0}% '.format(vol))
                        DeviceLogger.log('info', 'AudioChar', 'Volume sent to mobile app: {0}% '.format(vol))
                        volume_line = bytearray('Volume:'+ str(vol),'utf-8')
                        self._updateValueCallback(volume_line)
                        enddata = bytearray(b'enddata')
                        self._updateValueCallback(enddata)
                        callback(Characteristic.RESULT_SUCCESS)
                    except Exception as ex:
                        BluetoothLogger.log('info', 'AudioChar', 'Error sending file names to mobile app: ' + str(ex))
                        DeviceLogger.log('info', 'AudioChar', 'Error sending file names to mobile app: ' + str(ex))
                        self._updateValueCallback(array.array('B', [0] * 0))
            callback(Characteristic.RESULT_SUCCESS)
        except Exception as ex:
            BluetoothLogger.log('error', 'AudioChar', 'Error on write request: ' + str(ex))
            DeviceLogger.log('error', 'AudioChar', 'Error on write request: ' + str(ex))


    def play(self, filename):
        DeviceController.play_test_file(filename)
        BluetoothLogger.log('info', 'AudioChar', 'Playing {0} file'.format(filename))


    def stop(self, callback = None):
        DeviceController.stop_test_file_play()
        BluetoothLogger.log('info', 'AudioChar', 'Stoped playing audio')
        if callback != None:
            callback()

    def list_files(self):
        arr = []
        for root, dir, filenames in os.walk("/home/pi/device/Files"):
            for filename in filenames:
                BluetoothLogger.log('info', 'AudioChar', 'File discovered in Files folder: {0} '.format(filename))
                DeviceLogger.log('info', 'AudioChar', 'File discovered in Files folder: {0} '.format(filename))
                arr.append(filename)
        return arr
    
    def onSubscribe(self, maxValueSize, updateValueCallback):
        self._updateValueCallback = updateValueCallback

    def onUnsubscribe(self):
        self._updateValueCallback = None

    def convert_hex_to_ascii(self,h):
        chars_in_reverse = []
        while h != 0x0:
            chars_in_reverse.append(chr(h & 0xFF))
            h = h >> 8
        chars_in_reverse.reverse()
        return ''.join(chars_in_reverse)