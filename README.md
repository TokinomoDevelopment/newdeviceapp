# Tokinomo device app

### **Use Visual Studio Code and SFTP Extension to copy the code to your device**

### **To run the uncompiled code on any device you will need to install dependencies:**

#### **Automatic installation**:

bash ./install_dependencies.sh

#### **Manual installation**:

- Update: <br>
  sudo apt-get update
- Install setuptools <br>
  sudo apt-get install python3-setuptools
- Install pip3: <br>
  sudo apt-get install python3-pip
- Install NPi.GPIO <br>
  git clone https://github.com/Tungsteno74/NPi.GPIO
  cd NPi.GPIO  
  sudo apt-get install python3-dev
  sudo python3 setup.py install
- Install smbus:<br>
  pip3 install smbus
- Install arrow:<br>
  pip3 install arrow==0.17.0
- Install pyserial:<br>
  pip3 install pyserial
- Install ntplib:<br>
  pip3 install ntplib
- Install pybleno:<br>
  pip3 install pybleno
- Install pybluez:<br>
  sudo apt-get install libbluetooth-dev
  sudo pip3 install gatt
  sudo apt-get install python3-dbus
  pip3 install pytz
  <br><br>

### **To compile the project into a single executable you will need to follow these steps:**

- Get a 16 Gb micro sim card and write the Device 10000 image on it
- Boot a device with this card
- Modify the sftp.json password value to 'fa'
- Change import in board.py from "import NPi.GPIO as GPIO" to "import RPi.GPIO as GPIO"
- Run the following commands on device:

  - in /home/pi: sudo mkdir compile
  - cd compile
  - sudo /usr/bin/python3 -m PyInstaller /home/pi/newdeviceapp/tokinomo.py --onefile
  - sudo mkdir publish
  - cd publish
  - sudo mkdir nodeapp
  - sudo cp ~/compile/dist/tokinomo ~/compile/publish/nodeapp/tokinomo-node
  - sudo tar -zcvf pyapp.tar.gz ./nodeapp/
  - sudo chmod +777 pyapp.tar.gz

- Run the following commands on connected device:
  - scp root@<ip>:/home/pi/compile/publish/pyapp.tar.gz ~/pyapp.tar.gz (password: fa)

#### The pyapp.tar.gz archive can be copied to cloud and will be downloaded by the current device application if:

- Device is in Default capaign
- Current application version saved in /home/pi/device.txt differs from the application version in tbl_versions
- Device has has_updates set to on

#### 4.3.9 fixes:

- Corrupt config.json files are deleted and service restarted
- WiFi network can have an empty space in the SSID
- Master disconnects when Slave is down

#### 4.3.10 fixes:

- Canceled reboot if no activation for 30 min

#### 4.3.11 fixes:

- Data and bluetooth connections checked every 5 seconds and LED light changed accordingly
- Control messages are sent between Master and Slave every 5 minutes; if the paired device does not respond the sending device is set to not connected status
- When mobile app disconnected all executing activites are stopped
- Reimplemented custom logs: activations/traffic can be received on demand, general logs will be received based on calendar selection
- Implemented request reboot
- NanoPi serial is sent to cloud in initial info
- Autoconnect to tokinomo WiFi network (if present) when started
- Device checks device.txt file integrity and existance. If corrupted or not existing it copies backup device.txt file
- Fixed bug: debug mode active even if device on standby
- Fixed sensor checking bug: sensor checking is available while button is pressed in mobile app
- If battery dead device sets last time from alive file
- If battery dead device sets last time from GPRS cell date
- Fixed double connection threads issue
- Added client, rtc status variables
- Added ssh_client version to device file
- Audio is downloaded in 100K zip file chunks, merged and unzipped on device
- Fixed duration error which causes app to crash
- Fixed old software -> new software bridge during pending campaigns activation and traffic error
- APN, username, password ca be set from cloud
- Software can connect to deployment.tokinomo.com and development.tokinomo.com
- If device is in no sensor campaign and exiting clock reset it activates without restart
- Clock reset works on dialogue campaign
- Added Connection Thread Timer
- Deprecated Plug/Battery setting
- If campaign change, new settings, new audio and Slave device restart software 

LED COLORS:

| Data Connection            | Bluetooth Connection | LED STATUS     |
| -------------------------- | -------------------- | -------------- |
| None                       | None                 | Blue           |
| WiFi (connected to router) | None                 | Green          |
| GPRS (connected to cell)   | None                 | Blinking Green |
| None                       | Mobile app           | Blinking Blue  |
| WiFi (connected to router) | Mobile app           | Blinking Blue  |
| GPRS (connected to cell)   | Mobile app           | Blinking Blue  |
| None                       | Dialogue             | Violet         |
| WiFi (connected to router) | Dialogue             | White          |
| GPRS (connected to cell)   | Dialogue             | Blinking White |
