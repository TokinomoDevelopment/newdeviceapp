from subprocess import Popen, PIPE
import os
import json
import sys
import requests
import time
from collections import OrderedDict 
from datetime import datetime, timedelta 
from dateutil import tz as tzz
import threading
from random import choice
from random import randint

from const import Constants
from devicelogger import DeviceLogger
from rtclogger import RTCLogger
from device.board import Board 

class Status():

    def __init__(self):

        # paths
        self.path = "/home/pi/device/config.json"
        self.bkp_path = '/home/pi/device/backup/config.json'
        self.dev_path = '/home/pi/device/device.txt'
        self.bkp_dev_path = '/home/pi/device/backup/device.txt'
        self.usage_home_path = '/home/pi/device/usage/'
        self.apn_timestamp = '/home/pi/device/apn_timestamp.txt'
        self.usage_path_file = None

        # variables
        self.config = None
        self.device_id = None
        self.campaign_id = None
        self.url = Constants.app_url
        self.iot_provider = Constants.iot_provider
        self.device_connection = 0 # log compound (cloud and blt) connection
        self.device_volume = None
        self.mechanism_status = None
        self.files = None
        self.crt_file_index = 0 # audio files index 
        self.device_location = None
        self.app_version = None
        self.arduinoversion = None
        self.netlight = None
        self.modem = None 
        self.sim_id = None 
        self.modem_signal = None
        self.imei = None 
        self.modem_firmware = None
        self.provider = None
        self.mac = None
        self.ip_address = None
        self.nanopi_serial = None
        self.ssh_client = None
        self.rtc_issues = 0
        # store 6 values battery status for rolling average
        self.battery_status = []  
        self.cpu_temp = None
        self.amb_temp = None
        self.wifi_signal = None
        self.timezone = None
        self.force_location = 0
        self.libs = ''
        self.apn_time = 0
        self.daytime_start = None # time of day when the device is turned on
        self.daytime_end = None # time of day when the device is turned off
        self.device_on = None # False if device on stand by, mobile app connected or dialogue slave not connected
        self.turn_on_date = None # time of power on
        self.dialogue = None # store dialogue retrieved from cloud
        self.dialogue_list = [] # store dialogue options 
        self.delays_list = [] # store delays for each audio file in dialogue list
        self.dialogue_connected = False
        self.connected = False # set to True if paired device connected
        self.list_index = None # store dialogue list chosen randomly
        self.scanning = False # store scanning for Master
        self.mobile_connected = False # store connection to mobile app state
        self.observers = [] # store callbacks for scanning if Slave
        self.modem_status = False # while False modem is running AT commands so customprovider shouldn't run
        self.initial = 0 # set to 0 on init, 1 if initial needed to be sent, 2 after initial was sent
        self.force_audio = False # set to True if audio found in setting but not in Files
        DeviceLogger.log('info', 'Status', 'Initialised Status') 

    def initialize(self, clock):
        # load object parameters
        self.set_device_on(False)
        self.load_device()
        if clock:
            self.set_date_from_board()
        self.load_timezone()
        self.load_config()
        self.load_files()
        self.load_usage()
        self.load_rtc_status()
        self.record_turn_on()
        self.load_volume()
        self.set_standby_time()
        self.set_cpu_temp()
        self.set_amb_temp()
        self.set_wifi_signal()
        self.load_apn_timestamp()
        # do not turn device on if dialogue mode or in standby
        if self.get_setting('USE_DIALOG') == False and not self.check_standby_status():
            self.set_device_on(True)

    ##### SETTERS for device, config, usage

    def load_config(self):
        # Load config file or default settings + patterns based on audio files
        '''
        Load config 
        '''
        if not os.path.isfile(self.path):
            if not os.path.isfile(self.bkp_path):
                config = {}
                config['settings'] = Constants.default_settings
                config['patterns'] = []
                files = sorted(os.listdir('/home/pi/device/Files/'))
                for index, f in enumerate(files):
                    pattern = { "fileid": str(index),
                    "movement": "2",
                    "pattern": [],
                    "dialog": None, 
                    "dialog_type": "Question"  }
                    pattern['filename'] = f 
                    config['patterns'].append(pattern)
                config['timezone'] = "Europe/Bucharest"
                self.config = config
                DeviceLogger.log('warning', 'Status', 'No config file or backup config file found; loading "notdefined" settings and patterns')
                self.set_files_duration()
                self.set_campaign_id(0) 
                return 

            else:
                with open(self.bkp_path, "r") as f:
                    try:
                        config = json.load(f)
                    except Exception as ex:
                        DeviceLogger.log('error', 'Status', 'File {0} corrupted: '.format(f.name) + str(ex))             
                        p = Popen(['sudo rm -rf {0}'.format(f.name) ], stdout=PIPE, shell=True)
                        DeviceLogger.log('error', 'Status', 'Removed file and restarting service')             
                        p = Popen(['sudo systemctl restart tokinomo'], stdout=PIPE, shell=True)
                    self.config = config
                    with open(self.path, "w") as config_file:
                        config_file.write(json.dumps(config, indent=4, sort_keys=True))
                    DeviceLogger.log('warning', 'Status', 'No config file found. Loading settings from backup config file')
                    self.set_files_duration()
                if 'patterns' in self.config:
                    DeviceLogger.log('info', 'Status', 'Loading patterns from backup config file')
                else:
                    DeviceLogger.log('warning', 'Status', 'No patterns file found; need to download from cloud' )
                return

        with open(self.path, "r") as f:
            try:
                config = json.load(f)
            except Exception as ex:
                DeviceLogger.log('error', 'Status', 'File {0} corrupted: '.format(f.name) + str(ex))             
                p = Popen(['sudo rm -rf {0}'.format(f.name) ], stdout=PIPE, shell=True)
                DeviceLogger.log('error', 'Status', 'Removed file and restarting service')             
                p = Popen(['sudo systemctl restart tokinomo'], stdout=PIPE, shell=True)
            self.config = config
            DeviceLogger.log('info', 'Status', 'Loaded settings from config file')
            self.set_files_duration()
            if 'patterns' in self.config:
                DeviceLogger.log('info', 'Status', 'Loading patterns from config file')
            else:
                DeviceLogger.log('warning', 'Status', 'No patterns file found; need to download from cloud' )
            if self.get_setting('USE_DIALOG'):
                try:
                    self.set_dialogue(config['dialogue'])
                except Exception as ex:
                    DeviceLogger.log('error', 'Status', 'Use dialog enabled but no dialog list found: ' + str(ex))
            else:
                self.set_dialogue(None)

    def set_config(self, config):
        # Update config 
        self.config['settings'] = config
        DeviceLogger.log('info', 'Status', 'Loaded new settings downloaded from cloud')
        self.save_config('settings')
        self.save_backup_config('settings')

    def set_patterns(self, patterns):
        # set new pattern data
        self.config['patterns'] = patterns
        DeviceLogger.log('info', 'Status', 'Loaded new patterns downloaded from cloud')
        self.save_config('patterns')
        self.save_backup_config('patterns')

    def set_dialogue(self, dialogue):
        # set dialogue data
        self.config['dialogue'] = dialogue
        if dialogue != None:
            DeviceLogger.log('info', 'Status', 'Loaded dialogue')
            self.save_config('dialogue')
            self.save_backup_config('dialogue')
            self.dialogue_list = []
            self.delays_list = []
            for index,d in enumerate(dialogue):
                current_sequence = d['sequence']
                current_list =  [int(value) for value in current_sequence.split(',')]
                DeviceLogger.log('info', 'Status', 'Loaded dialogue list {0} : {1}'.format(index, current_list)) 
                self.dialogue_list.append(current_list)
                current_delays = d['delays']
                current_delays_list = [float(value) for value in current_delays.split(',')]
                self.delays_list.append(current_delays_list)
                DeviceLogger.log('info', 'Status', 'Loaded delays list {0} : {1}'.format(index, current_delays_list)) 
            if Status.get_setting('DIALOG_ROLE') == 'MASTER':
                self.select_dialogue_list()
            DeviceLogger.log('info', 'Status', 'Device in dialogue mode as ' + str(self.get_setting('DIALOG_ROLE')) )
        
    def select_dialogue_list(self):
        # randomly select one dialogue list to be played
        self.list_index = choice(range(len(self.dialogue_list)))
        DeviceLogger.log('info', 'Status', 'Randomly selected list {1} {0} for dialogue'.format(self.dialogue_list[self.list_index], self.list_index))

    def set_list_index(self, value):
        self.list_index = value

    def modify_major_setting(self, setting, value):
        self.config[setting] = value

    def load_device(self):
        # Load device file
        if not os.path.isfile(self.dev_path):
            DeviceLogger.log('warning', 'Status', 'No device file found. Checking backup folder')
            if not os.path.isfile(self.bkp_dev_path):
                DeviceLogger.log('warning', 'Status', 'No backup device file found; closing app')
            else:
                Popen(['sudo', 'cp',self.bkp_dev_path, self.dev_path]).wait()
                DeviceLogger.log('warning', 'Status', 'Copied device file from backup to main folder')

        config = None
        try:
            with open(self.dev_path, "r") as f:
                config = f.read()
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot find device file: ' + str(ex))
            DeviceLogger.log('error', 'Status', 'Closing app')
            Popen(['sudo systemctl stop tokinomo'], stdout=PIPE, shell=True)
 
        try:
            config = json.loads(config)
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Device file format error: ' + str(ex))
            try:
                with open(self.bkp_dev_path, "r") as f:
                    config = f.read()
                    config = json.loads(config)
                    Popen(['sudo', 'cp',self.bkp_dev_path, self.dev_path]).wait()
                    DeviceLogger.log('info', 'Status', 'Copied device file from backup to main folder')
            except Exception as ex:
                DeviceLogger.log('error', 'Status', 'Backup device file format error: ' + str(ex))
                DeviceLogger.log('error', 'Status', 'Closing app')
                os._exit(0)
                Popen(['sudo systemctl stop tokinomo'], stdout=PIPE, shell=True)

        DeviceLogger.log('info', 'Status', 'Loaded device file')

        try: 
            self.device_id = int(config.get('id'))
            self.campaign_id = int(config.get('campaign_id'))
            DeviceLogger.log('info', 'Status', 'Device set to ' + str(self.device_id) \
                            + ' and campaign to ' + str(self.campaign_id))
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Missing device id or campaign id: ' + str(ex))
            os._exit(0)

        self.app_version = Constants.app_version
        arduinoversion = config.get('arduinoversion')
        if arduinoversion == None:
            self.arduinoversion = Constants.arduinoversion
        else:
            self.arduinoversion = arduinoversion

        DeviceLogger.log('info', 'Status', 'App version: ' + str(self.app_version) \
                         + '; arduino version: ' + str(self.arduinoversion))

        if 'url' in config:
            self.url = config['url']
            DeviceLogger.log('info', 'Status', '!!! Device connects to: {0}'.format(self.url))

        if 'iot_provider' in config:
            self.iot_provider = config['iot_provider']
            # modify custom provider with new provider apn
            try:
                cmd = 'sudo sed -i ' + "'s/-T .*/-T {0}".format(self.iot_provider) + '"/' + "' /etc/ppp/peers/customprovider"
                p = Popen([cmd], stdout=PIPE, shell=True)
                DeviceLogger.log('info', 'Status', 'Wrote {0} to customprovider file'.format(self.iot_provider))
            except Exception as ex:
                DeviceLogger.log('error', 'Status', 'Cannot write {0} to customprovider file : {1}'.format(self.iot_provider, str(exe)))

        if 'linux_utils' in config:
            self.libs = config['linux_utils']

        if 'ssh_client' in config:
            self.ssh_client = config['ssh_client']

    def load_apn_timestamp(self):
        if os.path.isfile(self.apn_timestamp):
            try:
                with open(self.apn_timestamp, "r") as f:
                    self.apn_time = int(float(f.read()))
                    DeviceLogger.log('info', 'Status', 'Loaded APN timestamp')
            except Exception as ex:
                DeviceLogger.log('error', 'Status', 'Cannot load mobile app APN timestamp:' + str(ex))

    def set_apn_timestamp(self, value):
        self.apn_time = value
        if not os.path.isfile(self.apn_timestamp):
            os.popen('sudo touch {0}'.format(self.apn_timestamp))
        try:
            with open(self.apn_timestamp, "w") as f:
                f.write(str(value))
        except Exception as ex:
                DeviceLogger.log('error', 'Status', 'Cannot write apn timestamp:' + str(ex))

    def get_apn_timestamp(self):
        return self.apn_time 

    def load_rtc_status(self):
        try:
            self.rtc_file = '/home/pi/device/log/rtc/rtc_log'
            if os.path.isfile(self.rtc_file):
                try:
                    with open(self.rtc_file) as f:
                        for i, l in enumerate(f):
                            pass
                    self.rtc_issues = i + 1
                    DeviceLogger.log('warning', 'Status', 'RTC issues: ' + str(self.rtc_issues))
                except:
                    DeviceLogger.log('info', 'Status', 'No RTC issues')
            else:
                DeviceLogger.log('info', 'Status', 'No RTC issues')
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot load RTC status: ' + str(ex))

    def set_campaign_id(self, id):
        # set campaign id
        self.campaign_id = id
        DeviceLogger.log('info', 'Status', 'Set campaign id to ' + str(self.campaign_id))
        self.save_device_file()

    def load_files(self):
        # load file info in self.files variable
        # movement = 0 -> pattern    
        # movement = 2 -> random 
        # movement = 3 -> bounce
        movement_dict = { 0: 'pattern', 2: 'random', 3: 'bounce', 4: 'auto-generate'}
        temp = {}
        self.crt_file_index = 0
        for f in self.config['patterns']:  
            try:   
                size = os.stat('/home/pi/device/Files/' + f['filename']).st_size
                try:
                    key = int(f['fileid'])
                except Exception as ex:
                    key = randint(1, 1000)
                try:
                    temp[key] = [int(key), str(f['filename']), int(f['movement']), f['pattern'], f['duration']]
                except Exception as ex:
                    DeviceLogger.log('warning', 'Status', 'Cannot load {0}, reason: {1}'.format(f['filename'],ex))
            except Exception as ex:
                DeviceLogger.log('warning', 'Status', 'File {0} not found on device'.format(f['filename']))

        self.files = list(OrderedDict(sorted(temp.items())).values())
        for value in self.files:  
            pattern_str = ''
            if value[3] != []:
                pattern_str = ', pattern exists'
            DeviceLogger.log('info', 'Status', 'Loaded ' + str(value[1]) + ', id ' + str(value[0]) + ', duration ' + str(value[4]) + 's, ' + movement_dict[value[2]] + ' movement ' + pattern_str)

    def load_usage(self):
        # Load usage file
        self.usage_path = self.usage_home_path + str(self.campaign_id) 
        self.usage_path_file = self.usage_path + '/usage.json'
        if not os.path.isdir(self.usage_path):
            os.makedirs(self.usage_path)
            DeviceLogger.log('info', 'Status', 'Created usage campaign folder')
        else:
            DeviceLogger.log('info', 'Status', 'Campaign usage folder already exists')
        if os.path.isfile(self.usage_path_file) and self.campaign_id != 1:
            with open(self.usage_path_file, "r") as f:
                try:
                    self.usage = json.load(f)
                    DeviceLogger.log('info', 'Status', 'Usage and daystatus loaded from file')
                except:
                    self.usage = {
                        'status':[{'activations' : 0, 'trafic' : 0}],
                        'daystatus':[]
                    }
                    DeviceLogger.log('warning', 'Status', 'Usage file found but no vaild data present; usage and daystatus initialized to zero')
                    self.save_usage()
        else:
            self.usage = {
                'status':[{'activations' : 0, 'trafic' : 0}],
                'daystatus':[]
            }
            DeviceLogger.log('warning', 'Status', 'No usage file found; usage and daystatus initialized to zero')
            self.save_usage()
 
    def record_turn_on(self):
        # record device on
        try:
            self.date = Board.read_rtc_date().strftime("%Y-%m-%d %H:%M:%S")
            self.alive_path = self.usage_path + '/alive.json'
            if os.path.isfile(self.alive_path):
                with open(self.alive_path, "r+") as alive_file:
                    try:
                        alive = json.load(alive_file)
                    except Exception as ex:
                        alive = {}
                    finally:
                        alive[self.date] = self.date
                        alive_file.seek(0)
                        alive_file.write(json.dumps(alive, indent=4, sort_keys=True))
                        alive_file.truncate()
                        DeviceLogger.log('info', 'Status', 'Wrote device on timestamp')
            else:
                alive = {
                        self.date : self.date
                        }
                with open(self.alive_path, "w") as alive_file:
                    alive_file.write(json.dumps(alive, indent=4, sort_keys=True))
                    DeviceLogger.log('info', 'Status', 'Created ALIVE file')
                    DeviceLogger.log('info', 'Status', 'Wrote device on timestamp')
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot write turn on timestamp' + str(ex))

    def get_day_analytics(self):
        if "daystatus" in self.usage:
            daystatus = self.usage.get("daystatus")
            data = [v for v in daystatus if v['campaign'] == self.campaign_id]
            return data
        else:
            return []

    def heartbeat(self):
        # update value in dictionary with current time
        current_date = Board.read_rtc_date().strftime("%Y-%m-%d %H:%M:%S")
        try:
            with open(self.alive_path, "r+") as alive_file:
                alive = json.load(alive_file)
                alive[self.date] = current_date
                alive_file.seek(0)
                alive_file.write(json.dumps(alive, indent=4, sort_keys=True))
                alive_file.truncate()
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot write alive heartbeat: ' + str(ex))

    def get_alive_date(self):
        try:
            self.alive_path = self.usage_home_path + str(self.campaign_id) + '/alive.json' 
            with open(self.alive_path, "r+") as alive_file:
                alive = json.load(alive_file) 
                last_date = sorted(OrderedDict(alive).items())[-1][1]
                return datetime.strptime(last_date,'%Y-%m-%d %H:%M:%S')
        except Exception as ex:
            RTCLogger.log('error', 'Status', 'Cannot parse alive file to change board date: ' + str(ex))
            DeviceLogger.log('error', 'Status', 'Cannot parse alive file to change board date: ' + str(ex))

    def set_usage(self):
        pass

    def set_modem(self, value):
        self.modem = value 
    
    def set_sim_id(self, value):
        self.sim_id = value 
    
    def set_modem_signal(self, value):
        self.modem_signal = value

    def set_imei(self, value):
        self.imei = value

    def set_modem_firmware(self, value):
        self.modem_firmware = value

    def set_provider(self, value):
        self.provider = value

    def set_mac(self, value):
        self.mac = value

    def set_nanopi_serial(self, value):
        self.nanopi_serial = value

    def set_initial(self, value):
        self.initial = value

    def set_iot_provider(self, value):
        self.iot_provider = value

    def set_netlight(self):
        # set type of modem connection:
        # if interval is ~= 200 msec there is 4G connectivity
        # if interval is ~= 800 msec there is 2G/3G connectivity
        start = time.time()
        timer = time.time()
        line = Board.read_netlight()
        counter_200msec = 0
        while time.time() - start < 2:
            current_line = Board.read_netlight()
            if current_line != line:
                line = current_line
                interval = time.time()-timer
                if interval > 0.15 and interval < 0.25:
                    counter_200msec += 1
                timer = time.time()
        if counter_200msec >= 8:
            self.netlight = '4G'
        else:
            self.netlight = '2G/3G'
        DeviceLogger.log('info', 'Status', 'Netlight set to ' + str(self.netlight))

    def set_timezone(self, tz):
        self.timezone = tz
        DeviceLogger.log('info', 'Status', 'Software timezone variable set to ' + str(tz))

    def set_standby_time(self):
        # server sends time in -2 hours so we add it to both clock times
        self.daytime_start = self.get_setting('CLOCK_START_TIME') 
        self.daytime_end = self.get_setting('CLOCK_END_TIME') 
        DeviceLogger.log('info', 'Status', 'Clock start time set to ' + str(self.daytime_start.strftime("%H:%M:%S")))
        DeviceLogger.log('info', 'Status', 'Clock end time set to ' + str(self.daytime_end.strftime("%H:%M:%S")))

    def set_scanning(self, value):
        self.scanning = value
        # call binded methods only if not already running
        for callback in self.observers:
            callback()

    def set_mobile_connected(self, value):
        self.mobile_connected = value

    def set_modem_status(self, value):
        self.modem_status = value

    ##### OTHER SETTERS  
     
    def set_files_duration(self):  
        # adds time key to patterns
        if not 'patterns' in self.config:
            return
 
        # load data about files
        try:
            patterns = []
            files = sorted(os.listdir('/home/pi/device/Files/'))
            for f in files:
                p = next((d for d in self.config['patterns'] if d['filename'] == f),None)
                if not p:
                    DeviceLogger.log('error', 'Status', 'File {0} found on drive but not in patterns'.format(f))
                    p = Popen(['sudo rm -rf /home/pi/device/config.json'], stdout=PIPE, shell=True)
                    p = Popen(['sudo rm -rf /home/pi/device/backup/config.json'], stdout=PIPE, shell=True)
                    DeviceLogger.log('error', 'Status', 'Deleted config files and restarting app')
                    p = Popen(['sudo systemctl restart tokinomo'], stdout=PIPE, shell=True)
                stats=os.popen('soxi /home/pi/device/Files/'+ f + ' | grep Duration').readlines()
                for info in stats:
                    inf = info.strip().split(":")
                    duration = inf[1:4]
                    h = float(duration[0])
                    m = float(duration[1])
                    s = float(duration[2].split("=")[0])
                    s = m*60 + s
                p['duration'] = s
                DeviceLogger.log('info', 'Status', 'Set duration for {0}: {1}'.format(p['filename'],s))


            self.save_config('duration')
            self.save_backup_config('duration')

        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot determine files duration: ' + str(ex))

    def load_volume(self):
        # load % volume from linux
        p = Popen(["amixer sget 'Line Out' | grep 'Right:'"], stdout=PIPE, shell=True)
        out = str(p.stdout.read())
        words = out.split(' ')
        vol = words[6].replace('[','').replace(']','').replace('%','')
        self.device_volume = int(vol)

    def set_device_volume(self, volume):
        # set new device volume
        if int(volume) != self.device_volume:
            self.device_volume = int(volume)
            DeviceLogger.log('info', 'Status', 'Device volume set to '+ str(volume) + "%")
            dB_before = self.get_linux_dB_volume()
            os.popen('amixer sset "Line Out" {0}%'.format(volume))
            dB_after = self.get_linux_dB_volume()
            if dB_before != dB_after:
                os.popen('sudo alsactl store')
                DeviceLogger.log('info', 'Status', 'System volume set to '+ str(volume) + "%, " + dB_after)

    def set_mechanism_status(self, value):
        self.mechanism_status = value
        DeviceLogger.log('info', 'Status', 'Mechanism status set to ' + str(self.mechanism_status))

    def set_device_connection(self, connection):
        self.device_connection = connection

    def set_status(self, status_value):
        self.status = status_value

    def set_device_location(self, value):
        self.device_location = value

    def set_ip_address(self):
        try:
            self.ip_address = os.popen('wget -qO- http://ipecho.net/plain | xargs echo').read().rstrip()
            DeviceLogger.log('info', 'Status', 'Registered ip address: ' + self.ip_address)
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot get ip address: ' + str(ex))

    def set_device_location_app(self, location):
        self.device_location = str(location)
        DeviceLogger.log('info', 'Status', 'Registered new location from mobile app: ' + self.device_location)

    def set_cpu_temp(self):
        try:
            p = Popen(['echo $((`cat /sys/class/thermal/thermal_zone0/temp` / 1000))'],stdin=PIPE,stdout=PIPE,shell=True)
            temp = p.stdout.readline()
            self.cpu_temp = int(temp)
        except Exception as ex:
            pass       

    def set_amb_temp(self):
        self.amb_temp = Board.get_temp()

    def set_wifi_signal(self):
        self.wifi_signal = 'Not connected!'
        try:
            p = Popen(['iwconfig wlan0'],stdin=PIPE,stdout=PIPE,shell=True)
            for _ in range(6):
                p.stdout.readline()
            wsig = int(p.stdout.readline().split()[3][6:])
            if int(wsig) >= -50:
                self.wifi_signal = 'Excellent'
            elif int(wsig) > -70 and int(wsig) < -50:
                self.wifi_signal = 'Good'
            elif int(wsig) <= -70:
                self.wifi_signal = 'Weak'
        except Exception as ex:
            pass       


    def set_date_from_board(self):
        try:
            board_date = Board.read_rtc_date()
            if board_date.year >= 2021:
                # DEPRECATED: System time management migrated to linux
                # proc = Popen(['sudo date --set "{0}"'.format(board_date)], stdout=PIPE, stderr=PIPE ,shell=True)  
                # out, err = proc.communicate()
                # if err:
                #     DeviceLogger.log('info', 'Status', 'Cannot set system time from board time: ' + str(err))
                # else:
                #     DeviceLogger.log('info', 'Status', 'System time set to {0}'.format(board_date))
                # proc = Popen(['sudo', 'hwclock','-w'], stdout=PIPE, stderr=PIPE)  
                # out, err = proc.communicate()
                # if err:
                #     DeviceLogger.log('info', 'Status', 'Cannot set hardware time from system time: ' + str(err))
                # else:
                #     DeviceLogger.log('info', 'Status', 'Hardware clock set from system time')
                pass
            else:
                # set date from last alive logged date
                last_date = self.get_alive_date()
                system_date = datetime.now()
                if last_date < system_date:
                    self.set_date_to_board(system_date)
                    string = "system date"
                else:
                    self.set_date_to_board(last_date)
                    string = "alive file"
                # DEPRECATED: System time management migrated to linux
                # if board_date.year >= 2021:
                #     proc = Popen(['sudo date --set "{0}"'.format(board_date)], stdout=PIPE, stderr=PIPE ,shell=True)  
                #     out, err = proc.communicate()
                #     if err:
                #         DeviceLogger.log('info', 'Status', 'Cannot set system time from board time: ' + str(err))
                #     else:
                #         DeviceLogger.log('info', 'Status', 'System time set to {0}'.format(board_date))
                #     proc = Popen(['sudo', 'hwclock','-w'], stdout=PIPE, stderr=PIPE)  
                #     out, err = proc.communicate()
                #     if err:
                #         DeviceLogger.log('info', 'Status', 'Cannot set hardware time from system time: ' + str(err))
                #     else:
                #         DeviceLogger.log('info', 'Status', 'Hardware clock set from system time')
                RTCLogger.log('error', 'Status', 'Board time error: ' + str(board_date) + '; set date to ' + str(last_date) + ' from ' + str(string))
                DeviceLogger.log('error', 'Status', 'Board time error: ' + str(board_date) + '; set date to ' + str(last_date) + ' from ' + str(string))

        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot set system date from board date: ' + str(ex))
            
    def set_date_to_board(self, dt):
        Board.set_rtc_date(dt)

    def load_timezone(self):
        p = Popen(['timedatectl | grep Time'],stdin=PIPE,stdout=PIPE,shell=True)
        tz = str(p.stdout.readline()).split(' ')[9]
        self.timezone = tz
        DeviceLogger.log('info', 'Status', 'Software timezone set from system to: ' + self.timezone)
      
    def set_device_on(self, value):
        self.device_on = value
        if value:
            DeviceLogger.log('info', 'Status', 'Device state ON')
        else:
            DeviceLogger.log('info', 'Status', 'Device state OFF')

    def set_force_location(self, value):
        self.force_location = value

    def set_dialogue_connected(self, value):
        self.dialogue_connected = value

    def set_ssh_client(self, value):
        self.ssh_client = value

    def set_arduinoversion(self, value):
        self.arduinoversion = value
        try:
            with open(self.dev_path, "r") as f:
                config = f.read()
                config = json.loads(config)
                config['arduinoversion'] = self.arduinoversion
            with open(self.dev_path, "w") as f:
                f.write(json.dumps(config, indent=4, sort_keys=True)) 
            DeviceLogger.log('info', 'Status', 'Arduino version set to: ' + str(self.arduinoversion))
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Cannot load device file' + str(ex))


    # Getters

    def get_linux_dB_volume(self):
        # get real dB volume set in linux
        p = Popen(["amixer sget 'Line Out' | grep 'Right:'"], stdout=PIPE, shell=True)
        out = str(p.stdout.read())
        words = out.split(' ')
        dB = words[7].replace('[','').replace(']','')
        return dB

    def get_device_id(self):
        return self.device_id

    def get_campaign_id(self):
        return self.campaign_id

    def get_config(self):
        return self.config

    def get_device_volume(self):
        return self.device_volume

    def get_mechanism_status(self):
        return self.mechanism_status 

    def get_activations(self):
        try:
            return self.usage['status'][0]['activations']
        except:
            empty_dict = {
                "activations": 0,
                "trafic": 0
            }
            self.usage['status'].append(empty_dict)
            return 0

    def get_trafic(self):
        try:
            return self.usage['status'][0]['trafic']
        except:
            return 0

    def get_device_connection(self):
        return self.device_connection

    def get_status(self):
        return self.status

    def get_location(self):
        return self.device_location

    def get_major_setting(self, setting):
        if setting in self.config:
            return self.config[setting]  
        else:
            return False

    def get_setting(self,name):
        # get setting in config data based on name
        try:
            sp = next((d for d in self.config['settings'] if d['name'] == name),None)
            if sp == None:
                return False
            if sp['type'] == 'number':
                return float(sp['val'])
            if sp['type'] == 'bool':
                if sp['val'] == 'true':
                    return True
                else:
                    return False
            if sp['type'] == 'date':
                dt = self.str_to_date(sp['val'])
                return dt
            if sp['type'] == 'char':
                return sp['val']
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'No setting with name {0} found: '.format(name) + str(ex))

    def get_next_file_data(self):
        # get next file to be played + movement and pattern info
        try:
            data = self.files[self.crt_file_index] 
            f = data[1] 
            movement = data[2]
            pattern = data[3]
            self.crt_file_index += 1
            if self.crt_file_index == len(self.files):
                self.crt_file_index = 0
            return (f, movement, pattern)     
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot get next file to play')

    def get_file_by_id(self, id):
        # get next file to be played by id + movement and pattern info
        try:
            for data in self.files:
                if data[0] == id:
                    f = data[1] 
                    movement = data[2]
                    pattern = data[3] 
                    duration = data[4]
                    return (f, movement, pattern, duration)
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot get next file by id to play')

    def get_location(self):
        # get geolocation
        return self.device_location

    def get_modem(self):
        return self.modem 
    
    def get_sim_id(self):
        return self.sim_id  
    
    def get_modem_signal(self):
        return self.modem_signal

    def get_imei(self):
        return self.imei

    def get_modem_firmware(self):
        return self.modem_firmware

    def get_provider(self):
        return self.provider

    def get_mac(self):
        return self.mac

    def get_nanopi_serial(self):
        return self.nanopi_serial

    def get_initial(self):
        return self.initial

    def get_ip_address(self):
        return self.ip_address

    def get_list_index(self):
        return self.list_index

    def get_index(self):
        return self.index

    def set_battery_status(self, amps, volts):
        # amps: 
        # 10 bit arduino sensor, 5000 mV => 4.88 mV per read amps value
        # 2500 mV = 512 read amps value
        # 185 mV/A sensitivity (from datasheet)
        # amps = (2500 - ( amps * 4.88 )) / 1852
        volts = int(volts[:-3]) * 0.025488 # value obtained from calculations
        self.battery_status.append(volts)
        if len(self.battery_status) > 6:
            self.battery_status.pop(0)
        DeviceLogger.log('info', 'Status', 'Battery voltage: ' + '{:.2f}'.format(volts))

    def get_cpu_temp(self):
        self.set_cpu_temp()
        return self.cpu_temp

    def get_amb_temp(self):
        self.set_amb_temp()
        return self.amb_temp

    def get_wifi_signal(self):
        self.set_wifi_signal()
        return self.wifi_signal

    def get_settings_version(self):
        try:
            sp = next((d for d in self.config['settings'] if d['name'] == 'ALLOWED_DETECTIONS'),None)
            return sp['version']
        except Exception as ex:
            return 'N/A'

    def get_app_version(self):
        return self.app_version
    
    def get_arduinoversion(self):
        return self.arduinoversion

    def get_device_location(self):
        return self.device_location

    def get_timezone(self):
        return self.timezone

    def get_device_on(self):
        return self.device_on

    def get_daytime_start(self):
        return self.daytime_start

    def get_daytime_end(self):
        return self.daytime_end
 
    def get_battery_status(self):
        if len(self.battery_status) == 0:
            return 0
        else:
            sum = 0
            for value in self.battery_status:
                sum += value 
            return round(sum / len(self.battery_status), 2)

    def get_dialogue_list(self):
        return self.dialogue_list

    def get_delays_list(self):
        return self.delays_list

    def get_scanning(self):
        return self.scanning

    def get_mobile_connected(self):
        return self.mobile_connected
 
    def get_linux_version(self):
        p = Popen(['lsb_release -r'], stdout=PIPE, shell=True)
        out = p.stdout.read().decode().rstrip()
        linux = 'Ubuntu-' + out[-5:]
        p = Popen(['uname -r'], stdout=PIPE, shell=True)
        kernel = p.stdout.read().decode().rstrip()
        out = linux + ' ' + kernel
        return (out,linux,kernel)  

    def get_modem_status(self):
        return self.modem_status

    def get_url(self):
        return self.url

    def get_iot_provider(self):
        return self.iot_provider

    def get_ssh_client(self):
        return self.ssh_client

    def get_rtc_issues(self):
        return self.rtc_issues

    def get_force_location(self):
        return self.force_location

    def get_libs(self):
        return self.libs

    def get_dialogue_connected(self):
        return self.dialogue_connected

    def clear_battery_status(self):
        self.battery_status = []

    ## Others

    def save_config(self, reason):
        # save config file to device folder
        with open(self.path, "w") as f:
            try:
                f.write(json.dumps(self.config, indent=4, sort_keys=True)) 
                DeviceLogger.log('info', 'Status', 'Wrote config file to drive; reason: {0} change'.format(reason))
            except Exception as ex:
                DeviceLogger.log('error', 'Status', 'Cannot write config file to drive: ' + str(ex))

    def save_backup_config(self, reason):
        # save config file to backup device folder
        with open(self.bkp_path, "w") as f:
            try:
                f.write(json.dumps(self.config, indent=4, sort_keys=True)) 
                DeviceLogger.log('info', 'Status', 'Wrote backup config file to drive; reason: {0} change'.format(reason))
            except Exception as ex:
                DeviceLogger.log('error', 'Status', 'Cannot write backup config file to drive: ' + str(ex))
 
    def save_device_file(self, data = None):
        # update device file with id, version
        try:
            if data == None:
                with open(self.dev_path, "r") as f:
                    data = json.load(f)
                data['campaign_id'] = '{0}'.format(str(self.campaign_id))
            with open(self.dev_path, "w") as f:
                f.write(json.dumps(data, indent=4, sort_keys=True)) 
            with open(self.bkp_dev_path, "w") as f:
                f.write(json.dumps(data, indent=4, sort_keys=True)) 
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot update device file: ' + str(ex))

    def save_usage(self):
        # save config file to backup device folder
        try:
            with open(self.usage_path_file, "w") as f:
                f.write(json.dumps(self.usage, indent=4, sort_keys=True)) 
            DeviceLogger.log('info', 'Status', 'Wrote usage file to drive')
        except Exception as ex:
            DeviceLogger.log('error', 'Status', 'Cannot write usage file to drive: ' + str(ex))
 
    def increment_activations(self):
        # increment activations
        date = str(Board.read_rtc_date()).split()[0]
        date_split = date.split('-')
        dt = '{2}.{1}.{0}'.format(date_split[0],date_split[1],date_split[2])
        found = False
        for u in self.usage['daystatus']:
            if u['dt'] == dt:
                activations = int(u['activations']) + 1
                u['activations'] = activations
                found = True
                break
        if not found:
            new_dict = {
                "dt": dt,
                "campaign": self.campaign_id, 
                "trafic": 0,
                "activations": 1
            }
            self.usage['daystatus'].append(new_dict)
        activations = int(self.usage['status'][0]['activations']) + 1
        self.usage['status'][0]['activations'] = activations


    def increment_traffic(self):
        # increment traffic
        date = str(Board.read_rtc_date()).split()[0]
        date_split = date.split('-')
        dt = '{2}.{1}.{0}'.format(date_split[0],date_split[1],date_split[2])
        found = False
        for u in self.usage['daystatus']:
            if u['dt'] == dt:
                trafic = int(u['trafic']) + 1
                u['trafic'] = trafic
                found = True
                break
        trafic = int(self.usage['status'][0]['trafic']) + 1
        self.usage['status'][0]['trafic'] = trafic

    def get_day_analytics(self):
        if "daystatus" in self.usage:
            daystatus = self.usage.get("daystatus")
            data = [v for v in daystatus if v['campaign'] == self.campaign_id]
            return data
        else:
            return []

    def str_to_date(self, strdate):
        time_of_day = strdate.split('T')[1].split('.')[0]
        dt = datetime.strptime(time_of_day,'%H:%M:%S')
        return dt

    def check_standby_status(self):
        # check if device needs to be on or off due to clock reset
        if not Status.get_setting('USE_CLOCK_RESET'):
            return False
        daytime = Board.read_utc_date()
        daytime = daytime.replace(year = 1900, month = 1, day = 1)
        utc_offset = datetime.now(tzz.gettz(self.timezone)).utcoffset() 
        daytime = daytime + utc_offset
        if self.get_daytime_start() < daytime and daytime < self.get_daytime_end():   
            return False 
        else:
            return True

    def bind_to(self, callback):
        self.observers.append(callback)

    def reset_bind(self):
        self.observers = []
 
Status = Status()



