import os
import logging

from device.board import Board
from logger import general_logger
from logging.handlers import RotatingFileHandler
from const import Constants


class DeviceLogger():

    def __init__(self):
        # initiate object with locations
        self.path = '/home/pi/device/log' 
        self.full_name = '/home/pi/device/log/general/general_log'
        self.name = 'LOG'
        if not os.path.isdir(self.path):
            os.mkdir(self.path)
        if not os.path.isdir(self.path + '/general'):       
            os.mkdir(self.path + '/general')
        #formatter = logging.Formatter('%(name)s:%(levelname)s%(message)s')
        self.logger = general_logger(self.name, self.full_name)
        self.log('info', 'DeviceLogger', '------------------------------------------')
        self.log('info', 'DeviceLogger', '---- Version {0} (35) -----'.format(Constants.app_version))
        self.log('info', 'DeviceLogger', 'Initialised DeviceLogger')    

    def log(self, type, location, message):
        # log all types of messages
        date = Board.read_rtc_date()
        if type == 'info':
            self.logger.info( '{:<38}'.format('{1}:INFO [{0}]'.format(date,self.name)) + '{:<20}'.format('[{0}]'.format(location)) + message )   
        elif type == 'debug':
            self.logger.debug( '{:<38}'.format('{1}:DEBUG [{0}]'.format(date,self.name)) + '{:<20}'.format('[{0}]'.format(location)) + message )                         
        elif type == 'warning':
            self.logger.warning( '{:<38}'.format('{1}:WARNING [{0}]'.format(date,self.name)) + '{:<20}'.format('[{0}]'.format(location)) + message )   
        elif type == 'error':
            self.logger.error( '{:<38}'.format('{1}:ERROR [{0}]'.format(date,self.name)) +  '{:<20}'.format('[{0}]'.format(location)) + message )   

DeviceLogger = DeviceLogger()

     