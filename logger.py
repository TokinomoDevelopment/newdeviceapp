import logging
import sys

from logging.handlers import RotatingFileHandler

def general_logger(name, log_file, level=logging.DEBUG):
    # general logger
    logger = logging.getLogger(name)
    handler = RotatingFileHandler(filename=log_file, mode='a', maxBytes=250*1024, 
            backupCount=150, encoding=None, delay=0)
    logger.addHandler(handler)
    logger.setLevel(level)
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logging.getLogger("requests").setLevel(logging.WARNING)  
    return logger

def specific_logger(name, log_file, level=logging.INFO):
    # logger for bluetooth, connection, activations and traffic
    logger = logging.getLogger(name)
    logger.propagate = False
    handler = RotatingFileHandler(filename=log_file, mode='a', maxBytes=250*1024, 
            backupCount=150, encoding=None, delay=0)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger
 