import logging
import os

from device.board import Board
from logger import specific_logger
from status import Status

class UsageLogger():

    def initialize(self):
        self.usage_folder = '/home/pi/device/usage/{0}'.format(Status.get_campaign_id())
        self.activations = self.usage_folder + '/activations.log'
        self.trafic = self.usage_folder + '/trafic.log'
        self.act_name = 'ACT'
        self.tra_name = 'TRA'
        if not os.path.isfile(self.activations):
            os.popen('sudo touch {0}/activations.log'.format(self.usage_folder))
        if not os.path.isfile(self.trafic):
            os.popen('sudo touch {0}/trafic.log'.format(self.usage_folder))
        self.activations_logger = specific_logger(self.act_name, self.activations)
        self.trafic_logger = specific_logger(self.tra_name, self.trafic)

    def log_activations(self, type, location, message):
        date = Board.read_rtc_date()
        self.activations_logger.info( '{:<38}'.format('{1}:INFO [{0}]'.format(date,self.act_name)) + '{:<20}'.format('[{0}]'.format(location)) + message )

    def log_trafic(self, type, location, message):
        date = Board.read_rtc_date()
        self.trafic_logger.info( '{:<38}'.format('{1}:INFO [{0}]'.format(date,self.tra_name)) + '{:<20}'.format('[{0}]'.format(location)) + message ) 

UsageLogger = UsageLogger()

    