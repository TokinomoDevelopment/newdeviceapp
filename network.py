import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import json
import os
import shutil
import zipfile
from time import sleep, gmtime
from io import BytesIO
from datetime import datetime 
from subprocess import Popen, PIPE, STDOUT, check_output
import subprocess
import zlib
import gzip
import subprocess
import time
from dateutil import tz as tzz
import glob 
import base64
from zipfile import ZipFile

from status import Status
from devicelogger import DeviceLogger
from usagelogger import UsageLogger
from const import Constants
from device.controller import DeviceController
from device.board import Board
from ble.blemanager import BleManager
from rtclogger import RTCLogger

class Network(object):

    def __init__(self):

        self.platform, linux, kernel = Status.get_linux_version()
        self.auth_headers =  { 'X-API-KEY': Constants.api_key , \
            'User-Agent' : 'Tokinomo_{0}/v{1} {2}/{3} {4}'.format(Status.get_device_id(), Status.get_app_version(), linux, kernel, Status.get_libs())}
        self.dev_path = '/home/pi/device/device.txt'
        self.app_url = Status.get_url()
        self.deviceId = Status.get_device_id()
        self.force_file_update = False
        self.mp3_download_count = 0 # record number of tries for downloading audio
        self.arduino_download_count = 0 # record number of tries for downloading arduino update
        self.campaign_change = False # record if campaign change received from cloud
        self.used_dialog = False # record if campaign before update uses dialogue
        self.chunk_idx = 0 # record chunk index
        self.ble = BleManager()
        self.ble.start_server()
        self.running_custom_patch = False
        DeviceLogger.log('info', 'Network', 'Initialised Network')

    def download_files(self):
        # download campaign audio files
        force = True
        try:
            r = requests.get(self.app_url + 'utils/melody/{0}?force={1}'.format(self.deviceId, force), stream=True,timeout=20,headers=self.auth_headers,verify=False)
            if 'Content-Disposition' in r.headers:
                DeviceLogger.log('info', 'Network', 'Ready to download files; file size: {0}, header file size: {1}'.format(len(r.content),r.headers.get('cache-control')))
                filesize = len(r.content)
                checksize = int(r.headers.get('cache-control'))
                if(filesize != checksize or filesize == 0):
                    DeviceLogger.log('error', 'Network', 'File sizes do not match; restarting download')
                    self.download_files()
                else:
                    if r.headers['Content-Disposition'] == 'attachment; filename="files.zip"':
                        shutil.rmtree('/home/pi/device/Files', ignore_errors=True)
                        z = zipfile.ZipFile(BytesIO(r.content))
                        z.extractall("/home/pi/device/Files")
                        DeviceLogger.log('info', 'Network', 'Files extracted')
                        DeviceLogger.log('info', 'Network', 'Sent update ok message to cloud')
                        self.mp3_download_count = 0
                    else:
                        DeviceLogger.log('error', 'Network', 'Cannot extract files; restarting download')
                        self.download_files()
            else:
                DeviceLogger.log('error', 'Network', 'No files found on cloud')
                if self.mp3_download_count < 10:
                    DeviceLogger.log('info', 'Network', 'Retrying download {0}'.format(self.mp3_download_count))
                    self.mp3_download_count += 1
                    self.download_files()
                else:
                    self.mp3_download_count = 0
                    DeviceLogger.log('error', 'Network', 'Abandoned downloading files')
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Cannot post to cloud to get files: ' + str(ex))
            if self.mp3_download_count < 10:
                DeviceLogger.log('info', 'Network', 'Retrying download {0}'.format(self.mp3_download_count))
                self.mp3_download_count += 1
                self.download_files()
            else:
                self.mp3_download_count = 0
                DeviceLogger.log('error', 'Network', 'Abandoned downloading files')

    def download_files_wrapper(self):
        try:
            url = self.app_url + 'files/audio_download_chunked/{0}?force=true'.format(self.deviceId)
            response = requests.get(url, stream=True,timeout=20,headers=self.auth_headers,verify=False)
            response_json = response.json()
            try:
                numChunks = int(response_json['numChunks'])
                DeviceLogger.log('info', 'Network', 'Campaign has chunked audio data')
                chunk_idx_file = '/home/pi/device/FilesArchive/chunk_idx'
                files_path = '/home/pi/device/Files/'
                archive_path = '/home/pi/device/FilesArchive/'
                for file in os.scandir(files_path):
                    os.remove(file.path)
                if not os.path.isdir(archive_path):
                    os.mkdir(archive_path)
                zip_file = archive_path + 'archive.zip'
                DeviceLogger.log('info', 'Network', 'Audio split in {} chunks, beginning download ...'.format(numChunks))
                os.remove(zip_file)
                DeviceLogger.log('info', 'Network', 'Deleted files archive from previous download')
                decodedBytes = base64.b64decode(response_json['encodedData'])
                crc = zlib.crc32(decodedBytes)
                sleep_time = 0.1
                while self.chunk_idx < numChunks:
                    sleep(sleep_time)
                    if str(response_json['crc']) == str(crc):
                        if self.chunk_idx == 0:
                            f = open(zip_file, 'wb')
                        else:
                            f = open(zip_file, 'ab')                    
                        f.write(decodedBytes)
                        f.close()
                        DeviceLogger.log('info', 'Network', 'Wrote audio chunk {0} out of {1}'.format(self.chunk_idx+1, numChunks))
                        self.chunk_idx += 1 
                        sleep_time = 0.1
                    if self.chunk_idx < numChunks:
                        try:
                            url = self.app_url + 'files/audio_download_chunked/{0}?chunk={1}&force=true'.format(self.deviceId,self.chunk_idx)
                            response = requests.get(url, stream=True,timeout=20,headers=self.auth_headers,verify=False)
                            response_json = response.json()
                            decodedBytes = base64.b64decode(response_json['encodedData'])
                            crc = zlib.crc32(decodedBytes)
                        except:
                            DeviceLogger.log('info', 'Network', 'Error downloading chunk {0} out of {1}; retrying'.format(self.chunk_idx+1, numChunks))
                            sleep_time = 5
                            crc = 0
                try:
                    with ZipFile(zip_file, 'r') as zipObj:
                        zipObj.extractall(files_path)
                    DeviceLogger.log('info', 'Network', 'Unzipped audio files') 
                    confirmation_data = [ {
                                "action": "AudioDownloadConfirm"
                            }]
                    url = self.app_url + '/devices/action_confirm/{0}'.format(self.deviceId)
                    response = requests.post(url, json=confirmation_data, stream=True,timeout=20,headers=self.auth_headers,verify=False)
                    response_json = response.json()
                    self.chunk_idx = 0
                except Exception as ex:
                    DeviceLogger.log('error', 'Network', 'Archive unzip error: ' + str(ex))
                finally:
                    if os.path.isfile(chunk_idx_file):
                        os.remove(chunk_idx_file)
                        DeviceLogger.log('info', 'Network', 'Deleted chunk id file')
            except:
                DeviceLogger.log('info', 'Network', 'Campaign has single archive audio data')
                self.download_files()
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Cannot download chunk files: ' + str(ex))

    def download_patterns(self):
        # download settings 
        try:
            response = requests.get(self.app_url + 'utils/patterns/{0}'.format(Status.get_device_id()),timeout=20,headers=self.auth_headers, verify=False)
            response_json = response.json()
            DeviceLogger.log('info', 'Network', 'Downloaded new campaign patterns from cloud')
            Status.set_patterns(response_json)
        except Exception as ex:
                DeviceLogger.log('error', 'Network', 'Cannot download new campaign setting from cloud: ' + str(ex))
                DeviceLogger.log('error', 'Network', 'Retrying download')
                self.download_patterns()

    def select_archive_files(self, connection_type):
        # determine which file to send based on connection type
        # we send same data no matter the connection type, we'll see if we need this in the future
        pass

    def update_arduino(self, arduinoversion):
        try:
            DeviceLogger.log('info', 'Network', 'Trying to update arduino software')
            out = str(check_output(['wget --spider --server-response \
            {2}/utils/arduinotar/{0}?app={1}'.format(Status.get_device_id(), Status.get_app_version(), Status.get_url())], stderr=STDOUT, shell=True))
            idx = out.find('Content-Length:')
            size_on_cloud = int(out[idx:].split()[1][:-2])
            if not os.path.isdir('/home/pi/arduino'):
                os.mkdir('/home/pi/arduino')
            out = check_output(['sudo', 'wget', '--no-check-certificate', '-P',\
                '/home/pi/arduino', '-O', '/home/pi/arduino/arduino.tar.gz',\
                '{2}utils/arduinotar/{0}?app={1}'.format(Status.get_device_id(), Status.get_app_version(), Status.get_url())])
            proc = Popen(['sudo', 'ls', '-l', '/home/pi/arduino'], stdout=PIPE, stderr=PIPE)
            out, err = proc.communicate() 
            size_on_flash = int(str(out).split()[5])
            if size_on_cloud == size_on_flash:
                DeviceLogger.log('info', 'Network', 'Arduino update downloaded correctly')
                proc = Popen(['sudo', 'tar', 'xf', '/home/pi/arduino/arduino.tar.gz', '-C', '/home/pi', '--strip-components=1'], stdout=PIPE, stderr=PIPE)
                out, err = proc.communicate() 
                proc = Popen(['sudo', 'avrdude', '-c', 'linuxgpio', '-p', 'atmega328p', '-v', '-U', 'flash:w:/home/pi/arduino/program.hex:i'], stdout=PIPE, stderr=PIPE)
                out, err = proc.communicate() 
                Status.set_arduinoversion(arduinoversion)
                DeviceLogger.log('info', 'Network', 'ARDUINO UPDATED. REBOOTING...')
                p = Popen(['sudo reboot'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            else:
                if self.arduino_download_count < 5:
                    self.arduino_download_count += 1
                    DeviceLogger.log('error', 'Network', 'Arduino download failed; retrying ') 
                    self.update_arduino()
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Cannot update arduino software: ' + str(ex))  

    def send_full_logs(self, level, start, end):
    # send log files to cloud
        try:
            campaign_id = Status.get_campaign_id()
            usage_files = '/home/pi/device/usage/{0}/*'.format(campaign_id)
            general_log_files = '/home/pi/device/log/general/*'
            alive_file = '/home/pi/device/usage/{0}/alive.json'.format(campaign_id)
            tar = '/home/pi/device/usage/logs.tar.gz'
            path = '/home/pi/device/usage/logs/{0}'.format(campaign_id)
            os.system('sudo rm -rf {0}'.format(path))
            os.system('sudo mkdir {0}'.format(path))
            os.system('sudo cp {0} {1}'.format(alive_file, path))
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Error setting up FULL LOG paths and copying alive file: ' + str(ex))

        try:     
            if int(level) == 1 :
                for file in glob.glob(usage_files):
                    if 'trafic' in str(file) or 'activations' in str(file):
                        os.system('sudo cp {0} {1}'.format(file, path))
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Error copying traffic or activations files for FULL LOG: ' + str(ex))

        try:  
            start_date = datetime.strptime(start, '%Y-%m-%d').date()
            end_date = datetime.strptime(end, '%Y-%m-%d').date()
            for file in glob.glob(general_log_files):
                if 'general' in str(file):
                    file_date = datetime.fromtimestamp(os.stat(file).st_mtime).date()
                    if start_date <= file_date and file_date <= end_date:
                        os.system('sudo cp {0} {1}'.format(file, path))
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Error copying general files for FULL LOG: ' + str(ex))

        try:  
            proc = Popen(['sudo','tar', '-czf', tar, path],stdout=PIPE,stderr=PIPE)
            out, err = proc.communicate()
            multipart_form_data = {
                        'files': ('logs.tar.gz', open(tar, 'rb')),
                        'id': (None, str(Status.get_device_id()))
                    }
            response = requests.post(self.app_url + 'utils/logs', files=multipart_form_data,timeout=20,headers=self.auth_headers, verify=False)
            DeviceLogger.log('info', 'Network', 'Created FULL LOG archive message; sending to cloud' )
            response_json = response.json()
            if response_json == True:
                DeviceLogger.log('info', 'Network', 'Cloud received FULL LOG files')
            else:
                DeviceLogger.log('info', 'Network', 'Cloud did NOT receive FULL LOG files')
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'FULL LOG files error: ' + str(ex))


    def send_alive_file(self):
        # send alive file to cloud
        try: 
            campaign_id = Status.get_campaign_id()
            alive_file = '/home/pi/device/usage/{0}/alive.json'.format(campaign_id)
            tar = '/home/pi/device/usage/logs.tar.gz' 
            path = '/home/pi/device/usage/logs/{0}'.format(campaign_id)
            os.system('sudo rm -rf {0}'.format(path))
            os.system('sudo mkdir -p {0}'.format(path))
            os.system('sudo cp {0} {1}'.format(alive_file, path))
            proc = Popen(['sudo', 'tar', '-czvf', tar, path],stdout=PIPE,stderr=PIPE)
            out, err = proc.communicate()
            multipart_form_data = {
                        'files': ('logs.tar.gz', open(tar, 'rb')),
                        'id': (None, str(Status.get_device_id()))
                    }
            response = requests.post(self.app_url + 'utils/logs', files=multipart_form_data,timeout=20,headers=self.auth_headers, verify=False)
            DeviceLogger.log('info', 'Network', 'Created ALIVE log archive message; sending to cloud' )
            response_json = response.json()
            if response_json == True:
                DeviceLogger.log('info', 'Network', 'Cloud received ALIVE log file')
            else:
                DeviceLogger.log('info', 'Network', 'Cloud did NOT receive ALIVE log file')
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'ALIVE log file error: ' + str(ex))
 
    def stop_bluetooth(self):
        DeviceController.set_dialogue_index(0)
        proc = Popen(['sudo service bluetooth stop' ],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
        out, err = proc.communicate()  
        sleep(0.5)

    def run_custom_patch(self):
        DeviceLogger.log('info', 'Network', 'CUSTOM PATCH REQUESTED')
        rc = subprocess.call(['which', 'screen'])
        if rc == 0:
            DeviceLogger.log('info', 'Network', 'SCREEN INSTALLED')
        else:
            DeviceLogger.log('info', 'Network', 'NEED TO INSTALL SCREEN')
            p = Popen(['nohup sudo apt install screen -y'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            proc,err = p.communicate()  
            if err:
                self.running_custom_patch = False
                DeviceLogger.log('error', 'Network', 'Error running custom patch')
                return
            DeviceLogger.log('info', 'Network', 'Screen install process log: ' + str(proc))
            DeviceLogger.log('info', 'Network', 'Screen install error log: ' + str(err))
            sleep(10)
        p = Popen(['sudo screen -dm bash -c "wget --no-check-certificate -O /tmp/custompatch.txt\
        https://cloud.tokinomo.com/datacenter/devicepatches/custompatch.txt\
        && sudo mv /tmp/custompatch.txt /tmp/custompatch.sh && sudo bash /tmp/custompatch.sh &&\
        sudo rm -f /tmp/custompatch.sh"'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
        proc,err = p.communicate()  
        if err:
            self.running_custom_patch = False
            DeviceLogger.log('error', 'Network', 'Error running custom patch')
            return
        DeviceLogger.log('info', 'Network', 'Main patch process log: ' + str(proc))
        DeviceLogger.log('info', 'Network', 'Main patch error log: ' + str(err))

    def write_apn(self, apn):
        # set iot provider in device.txt
        config = None
        try:
            with open(self.dev_path, "r") as f:
                config = f.read()
                config = json.loads(config)
                config['iot_provider'] = apn
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Cannot load device file:' + str(ex))
        try:
            Status.save_device_file(config)
            DeviceLogger.log('info', 'Network', 'Wrote new iot provider in device file')
        except Exception as ex:
            DeviceLogger.log('info', 'Network', 'Cannot update iot provider in device file: ' + str(ex))

    def set_customprovider(self, apn, user, password):
        if user == '' or password == '':
            Popen(['sudo','/home/pi/device/modemconn/createcustomppp', apn]).wait()
            self.write_apn(apn)
            # modify custom provider with new provider apn
            try:
                cmd = 'sudo sed -i ' + "'s/-T .*/-T {0}".format(apn) + '"/' + "' /etc/ppp/peers/customprovider"
                p = Popen([cmd], stdout=PIPE, shell=True)
                DeviceLogger.log('info', 'Network', 'Wrote {0} to customprovider file'.format(apn))
            except Exception as ex:
                DeviceLogger.log('error', 'Network', 'Cannot write {0} to customprovider file : {1}'.format(apn, str(ex)))
        else:
            Popen(['sudo','/home/pi/device/modemconn/createcustomppp', apn,'auth', user, password]).wait()
            self.write_apn(apn)
            DeviceLogger.log('info', 'ConnectionChar', 'Iot provider set to: {0}, username set to: {1}, password set to: {2}'.format(apn, user, password))

    def update(self, connection_type):
        self.mp3_download_count = 0
        self.arduino_download_count = 0
        # send modem info first connection after restart
        if Status.get_initial() == 1:
            try:
                initial_data = {
                    'device_id': (None, str(Status.get_device_id())),
                    'modem': (None, str(Status.get_modem())),
                    'sim_id': (None, str(Status.get_sim_id())), 
                    'modem_signal': (None, str(Status.get_modem_signal())),
                    'imei': (None, str(Status.get_imei())),
                    'mac': (None, str(Status.get_mac())),
                    'ip_address': (None, str(Status.get_ip_address())),
                    'linux_version' : (None, str(self.platform)),
                    'modem_firmware' : (None, str(Status.get_modem_firmware())),
                    'gprs_provider' : (None, str(Status.get_provider())),
                    'nanopi_serial' : (None, str(Status.get_nanopi_serial())),
                    'ssh_client' : (None, str(Status.get_ssh_client())),
                    'rtc_issues' : (None, str(Status.get_rtc_issues())),
                    'iot_provider' : (None, str(Status.get_iot_provider()))
                } 
                sleep(1)

                DeviceLogger.log('info', 'Network', 'Created DEVICE INFO message; sending to cloud')
                response = requests.post(self.app_url + 'utils/initial', files=initial_data, timeout=20, headers=self.auth_headers, verify=False)

                response_json = response.json()
                print(json.dumps(response_json, indent=1)) 

                if response.status_code == 200:
                    DeviceLogger.log('info', 'Network', 'Cloud received DEVICE INFO data' )
                    Status.set_initial(2)
                    if int(response_json['request_sync']) == 1:
                        # sync device data with data received from cloud
                        sync_data = {'device_id': (None, str(Status.get_device_id())) }
                        response = requests.post(self.app_url + 'utils/sync_device', files=sync_data, timeout=20, headers=self.auth_headers, verify=False)
                        response_json = response.json()
                        # add new sync conditions if needed
                    if int(response_json['custom_patch']) == 1:  
                        if not self.running_custom_patch:
                            self.run_custom_patch()
                            self.running_custom_patch = True
                        else:
                            DeviceLogger.log('info', 'Network', 'Already running custom patch...')
                    if int(response_json['general_patch']) == 1:
                        DeviceLogger.log('info', 'Network', 'GENERAL PATCH REQUESTED')
                        rc = subprocess.call(['which', 'screen'])
                        if rc == 0:
                            DeviceLogger.log('info', 'Network', 'SCREEN INSTALLED')
                        else:
                            DeviceLogger.log('info', 'Network', 'NEED TO INSTALL SCREEN')
                            p = Popen(['nohup sudo apt install screen -y'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                            proc,err = p.communicate()  
                            DeviceLogger.log('info', 'Network', 'Screen install process log: ' + str(proc))
                            DeviceLogger.log('info', 'Network', 'Screen install error log: ' + str(err))
                            sleep(10)
                        p = Popen(['sudo screen -dm bash -c "wget --no-check-certificate -O /tmp/generalpatch.txt\
                        https://cloud.tokinomo.com/datacenter/devicepatches/generalpatch.txt\
                        && sudo mv /tmp/generalpatch.txt /tmp/generalpatch.sh && sudo bash /tmp/generalpatch.sh &&\
                        sudo rm -f /tmp/generalpatch.sh"'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                        proc,err = p.communicate()  
                        DeviceLogger.log('info', 'Network', 'Main general process log: ' + str(proc))
                        DeviceLogger.log('info', 'Network', 'Main general error log: ' + str(err))
                else:
                    DeviceLogger.log('error', 'Network', 'Cloud did NOT receive DEVICE INFO data') 
            except Exception as ex:
                DeviceLogger.log('error', 'Network', 'DEVICE INFO status message error: ' + str(ex))

        # send activations, traffic data for graphic representation
        try:
            data = Status.get_day_analytics()
            response = requests.post(self.app_url + 'utils/logtrafic', json={"id":self.deviceId,"campaign_id":Status.get_campaign_id(),"trafic_data":data},headers=self.auth_headers, verify=False)
            DeviceLogger.log('info', 'Network', 'Created LOG TRAFFIC message; sending to cloud' )
            if response.status_code == 200:
                DeviceLogger.log('info', 'Network', 'Cloud received LOG TRAFFIC message' )
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'LOG TRAFFIC message error: ' + str(ex))

        # evaluate cloud status and update where necessary
        try:    
            multipart_form_data = {
                'id': (None, str(Status.get_device_id())), 
                'campaign_id': (None, str(Status.get_campaign_id())),
                'sound': (None, str(Status.get_device_volume())),
                'mechanism': (None, str(Status.get_mechanism_status())),
                'product': (None, str(Status.get_mechanism_status())),
                'activations': (None, str(Status.get_activations())),
                'trafic': (None, str(Status.get_trafic())),
                'battery': (None, str(Status.get_battery_status())),
                'cputemp': (None, str(Status.get_cpu_temp())),
                'ambtemp': (None, str(Status.get_amb_temp())),
                'signal':(None, str(Status.get_modem_signal())),
                'connection':(None, str(Status.get_device_connection())),
                'wifi':(None, str(Status.get_wifi_signal())),
                'settings':(None, str(Status.get_settings_version())),
                'version':(None, str(Status.get_app_version())),
                'arduino':(None, str(Status.get_arduinoversion())),
                'location':(None, str(Status.get_device_location())),
                'force_location':(None, str(Status.get_force_location()))
            }
            DeviceLogger.log('info', 'Network', 'Created STATUS message; sending to cloud' )
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'STATUS message error: ' + str(ex))
          
        response = requests.post(self.app_url + 'utils/status', data=multipart_form_data,timeout=20,headers=self.auth_headers, verify=False)

        if response:
            DeviceLogger.log('info', 'Network', 'Cloud received STATUS message' )
        else:
            DeviceLogger.log('error', 'Network', 'No cloud STATUS message received')
            return

        response_json = response.json()
        print(json.dumps(response_json, indent=1))

        # dialog status
        self.campaign_change = False
        if Status.get_setting('USE_DIALOG'):
            self.used_dialog = True
        else:
            self.used_dialog = False

        # run custom patch
        if 'custom_patch' in response_json and int(response_json['custom_patch']) == 0 and \
            self.running_custom_patch == True:
            self.running_custom_patch = False
            DeviceLogger.log('info', 'Network', 'Custom patch ran succesfully!')

        if 'custom_patch' in response_json and int(response_json['custom_patch']) == 1 and \
            (Status.get_device_connection() == 1 or Status.get_device_connection() == 5 or Status.get_device_connection() == 7): 
            if not self.running_custom_patch:
                self.run_custom_patch()
                self.running_custom_patch = True
            else:
                DeviceLogger.log('info', 'Network', 'Already running custom patch...')

        # send logs
        if 'logs_on' in response_json:
            if int(response_json['logs_on']) == 0:
                self.send_alive_file()
            if int(response_json['logs_on']) == 1:
                self.send_full_logs(response_json['logging_level'],response_json['logging_period_start'],response_json['logging_period_end'] )

        # reboot if requested
        try:
            if 'reboot_request' in response_json and int(response_json['reboot_request']) == 1 \
            and Status.get_initial() == 2:
                DeviceLogger.log('info', 'Network', 'REBOOT REQUESTED. REBOOTING...')
                p = Popen(['sudo reboot'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                out,err = p.communicate() 
        except Exception as ex:
            DeviceLogger.log('error', 'Network', 'Reboot checking error: ' + str(ex))


        # handle campaign change 
        if 'campaignId' in response_json and int(response_json['campaignId']) != Status.get_campaign_id():
            
            # if previous campaign uses dialogue store this information to reset bluetooth

            self.campaign_change = True
            DeviceController.stop_device()
            DeviceController.set_sensor_stopped(True)

            Status.set_campaign_id(int(response_json['campaignId']))

            Status.set_config(response_json['settings']) 
            # self.download_files()
            self.download_files_wrapper()
            #self.download_patterns() we can download patterns explicitly too
            Status.set_patterns(response_json['patterns'])

            # load new campaign data
            Status.set_files_duration()
            Status.load_files()
            Status.load_usage()
            Status.record_turn_on()
            self.campaign_change = True
            DeviceController.set_waiting_time()
            # alive, activations, traffic need to be reinitialised in new campaign log folder
            Status.record_turn_on()
            UsageLogger.initialize()
            if Status.get_setting('USE_DIALOG'):
                Status.set_dialogue(response_json['dialogue'])
                self.stop_bluetooth()
            else:
                Status.set_dialogue(None)
                if self.used_dialog:
                    self.stop_bluetooth()
                else:
                    self.ble.restartBluetooth()
            Status.set_dialogue_connected(False)
            
        # handle settings change if same campaign
        if 'settings' in response_json and not self.campaign_change:

            if not DeviceController.get_sensor_stopped():
                DeviceController.stop_device()
                DeviceController.set_sensor_stopped(True)
            Status.set_config(response_json['settings'])
            Status.set_standby_time()
            DeviceController.set_waiting_time()
            #  restart advertising if role changed 
            if 'dialogue' in response_json:
                Status.set_dialogue(response_json['dialogue'])
            if self.used_dialog:
                self.stop_bluetooth()

        # handle files and patterns update if same campaign
        # IMPORTANT: pattern and files change are inter-dependent, 
        # they need to be downloaded at the same time
        if 'has_files' in response_json and int(response_json['has_files']) == 1 and not self.campaign_change:
            
            if not DeviceController.get_sensor_stopped():
                DeviceController.stop_device()
                DeviceController.set_sensor_stopped(True)
            
            #self.download_files()
            self.download_files_wrapper()
            Status.set_patterns(response_json['patterns'])
            Status.set_files_duration()
            Status.load_files()

        if 'volume' in response_json:
            Status.set_device_volume(response_json['volume'])

        if 'keepmodemon' in response_json:
            config = Status.get_config()
            if 'keepmodemon' in config and config['keepmodemon'] == response_json['keepmodemon']:
                pass
            else:
                Status.modify_major_setting('keepmodemon', response_json['keepmodemon'])
                DeviceLogger.log('info', 'Network', 'Set keepmodemon value to ' + str(config['keepmodemon']))
                Status.save_config('keepmodemon setting')
                Status.save_backup_config('keepmodemon setting')

        if 'debugmode' in response_json:
            config = Status.get_config()
            if 'debugmode' in config and config['debugmode'] == response_json['debugmode']:
                pass
            else:
                Status.modify_major_setting('debugmode',response_json['debugmode'])
                DeviceLogger.log('info', 'Network', 'Set debugmode value to ' + str(config['debugmode']))
                Status.save_config('debugmode setting')
                Status.save_backup_config('debugmode setting')

        if Status.get_timezone() != response_json['timezone']:
            tz = response_json['timezone']    
            p = Popen(['sudo rm /etc/localtime'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            out,err = p.communicate() 
            p = Popen(['sudo ln -s /usr/share/zoneinfo/{0} /etc/localtime'.format(tz)],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            out,err = p.communicate() 
            DeviceLogger.log('info', 'Network', 'System timezone set to: ' + tz)
            Status.set_timezone(tz)

        if 'servertime' in response_json:
            server_epoch = int(response_json['servertime'][6:-2])//1000
            # DEPRECATED: System time management migrated to linux
            # p = Popen(['date +%s'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            # out,err = p.communicate() 
            # utc_epoch = int(str(out)[2:-3])
            # if abs(server_epoch - utc_epoch) > 2:
            #     p = Popen(['sudo date -s @{0}'.format(server_epoch)],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            #     out,err = p.communicate()       
            #     DeviceLogger.log('info', 'Network', 'Changed system date')  
            utc_offset = datetime.now(tzz.gettz(Status.get_timezone())).utcoffset()  
            board_unixtime = (Board.read_rtc_date() - datetime(1970, 1, 1)).total_seconds() - utc_offset.total_seconds()
            if abs(server_epoch - board_unixtime) > 60:
                utc = datetime.fromtimestamp(server_epoch)
                Status.set_date_to_board(utc)
                DeviceLogger.log('info', 'Network', 'Changed board date')  

        if 'apn_ts' in response_json and int(response_json['apn_ts']) != 0 and ( Status.get_device_connection() == 1 or \
        Status.get_device_connection() == 5 or Status.get_device_connection() == 7):
            if Status.get_apn_timestamp() < int(response_json['apn_ts']):
                try:
                    self.set_customprovider(response_json['apn_provider'], response_json['apn_username'], response_json['apn_password'])
                    Status.set_apn_timestamp(int(response_json['apn_ts']))
                    Status.set_iot_provider(response_json['apn_provider'])
                    initial_data = {
                        'device_id': (None, str(Status.get_device_id())),
                        'iot_provider' : (None, str(Status.get_iot_provider())),
                        'apn_ts' : (None, str(Status.get_apn_timestamp()))
                    } 
                    sleep(1)
                    DeviceLogger.log('info', 'Network', 'Created IOT PROVIDER INFO message; sending to cloud')
                    response_init = requests.post(self.app_url + 'utils/initial', files=initial_data, timeout=20, headers=self.auth_headers, verify=False)
                    response_init_json = response_init.json()
                    print(json.dumps(response_init_json, indent=1)) 
                except Exception as ex:
                    DeviceLogger.log('error', 'Network', 'Reboot checking error: ' + str(ex))
            elif Status.get_apn_timestamp() > int(response_json['apn_ts']):
                try:
                    initial_data = {
                        'device_id': (None, str(Status.get_device_id())),
                        'iot_provider' : (None, str(Status.get_iot_provider())),
                        'apn_ts' : (None, str(Status.get_apn_timestamp()))
                    } 
                    sleep(1)
                    DeviceLogger.log('info', 'Network', 'Created IOT PROVIDER INFO message; sending to cloud')
                    response_init = requests.post(self.app_url + 'utils/initial', files=initial_data, timeout=20, headers=self.auth_headers, verify=False)
                    response_init_json = response_init.json()
                    print(json.dumps(response_init_json, indent=1)) 
                except Exception as ex:
                    DeviceLogger.log('error', 'Network', 'Reboot checking error: ' + str(ex))

        if response_json['appversion'] != Status.get_app_version() and int(response_json['has_updates']) == 1 \
            and Status.get_device_connection() == 1 and Status.get_campaign_id() == 1:
            old_version = Status.get_app_version()
            new_version = response_json['appversion']
            try:
                DeviceLogger.log('info', 'Network', 'Trying to update device software')
                out = check_output(['sudo', 'wget', '--no-check-certificate', '-O', '/home/pi/device/update/pyapp.tar.gz' ,  self.app_url + 'utils/pyupdate/{0}'.format(Status.get_device_id())])
                DeviceLogger.log('info', 'Network', 'Downloaded {0} app version'.format(new_version))
                try:
                    DeviceController.stop_device()
                    DeviceController.set_sensor_stopped(True) 
                    Status.set_device_on(False)
                    sleep(3)   
                    out = check_output(['sudo', 'tar', 'xf', '/home/pi/device/update/pyapp.tar.gz', '-C', '/home/pi', '--strip-components=1'])
                    DeviceLogger.log('info', 'Network', 'Replaced {0} app version with {1}'.format(old_version, new_version))
                    out = check_output(['sudo', 'reboot'])
                except Exception as ex:
                    DeviceLogger.log('error', 'Network', 'Cannot untar new app version: ' + str(ex))  
            except Exception as ex:
                DeviceLogger.log('error', 'Network', 'Cannot update device software: ' + str(ex))  

        if response_json['arduinoversion'] != Status.get_arduinoversion() and response_json['has_updates'] == 1\
            and Status.get_device_connection() == 1 and Status.get_campaign_id() == 1:
            self.update_arduino(response_json['arduinoversion'])

        # if sensor stopped then something was modified during this cloud connection
        if DeviceController.get_sensor_stopped():
            # if campaign uses motion sensor turn sensor on:
            if Status.get_setting('USE_MOTION_SENSOR'):
                DeviceController.set_sensor_stopped(False)
            # if dialogue campaing leave device off for pairing
            if Status.get_setting('USE_DIALOG'):
                Status.set_device_on(False)
        # if normal campaign turn device on if device off and not in standby
        if not Status.check_standby_status() and not Status.get_setting('USE_DIALOG') \
            and not Status.get_device_on():
                Status.set_device_on(True)
 
        # if campaign change, new settings, new audio and Slave device restart software 
        if (self.campaign_change or 'settings' in response_json or ('has_files' in response_json and int(response_json['has_files']) == 1)) and\
            Status.get_setting('DIALOG_ROLE') == 'SLAVE':   
            DeviceLogger.log('info', 'Network', 'Slave device has new settings; RESTARTING SOFTWARE')
            p = Popen(['sudo systemctl restart tokinomo'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)


