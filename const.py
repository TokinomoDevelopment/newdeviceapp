class Constants:
 
    # cloud connection credentials
    
    app_url = 'https://cloud.tokinomo.com/api/index.php/'
    api_key =  '{EC309241-B80B-321E-1CF0-66E2E8A3F487}' 
    iot_provider = 'internet.cheeriot.com'
    app_version = '4.3.11'
    arduinoversion = '1.1.3'

    # retry cloud connection times (seconds)
    short_time = 30
    long_time = 3600

    # record traffic after x seconds 
    traffic_timer_limit = 3
 
    default_settings = [{
            "comment": "number of allowed motions to be detected before WAITING_TIME pause",
            "group": "Detection",
            "id": "31791",
            "name": "ALLOWED_DETECTIONS",
            "ord_index": None,
            "type": "number",
            "val": "2",
            "version": "notdefined"
        },
        {
            "comment": "Time zone difference in hours from UTC",
            "group": "General",
            "id": "31811",
            "name": "UTC_DIFF",
            "ord_index": None,
            "type": "number",
            "val": "3",
            "version": "notdefined"
        },
        {
            "comment": "If true, the swing motion will be randomized",
            "group": "Movement",
            "id": "31805",
            "name": "USE_RANDOM_MOVEMENT",
            "ord_index": None,
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "If patterns are defined for each melody, the movement will use that.",
            "group": "Movement",
            "id": "31793",
            "name": "USE_PATTERN_MOVEMENT",
            "ord_index": None,
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "Use campaign with dialog feature",
            "group": "Movement",
            "id": "31812",
            "name": "USE_DIALOG",
            "ord_index": None,
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "Movement and audio will start with slow increasing speed and volume ",
            "group": "Movement",
            "id": "31814",
            "name": "SLOW_START",
            "ord_index": None,
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "set true if we want random play",
            "group": "General",
            "id": "31804",
            "name": "RANDOM_PLAY",
            "ord_index": None,
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "pause between playng succesive tracks (milisec) (no motion sensor)",
            "group": "Loop",
            "id": "31800",
            "name": "PAUSE_BETWEEN_TRACKS",
            "ord_index": None,
            "type": "number",
            "val": "5000",
            "version": "notdefined"
        },
        {
            "comment": "starting direction of movement: 1=forward first, 2=backward first",
            "group": "Motor",
            "id": "31796",
            "name": "MOTION_STARTING_DIR",
            "ord_index": None,
            "type": "number",
            "val": "1",
            "version": "notdefined"
        },
        {
            "comment": "pause between loops in continuos mode (milisec)  (no motion sensor)",
            "group": "Loop",
            "id": "31799",
            "name": "LOOP_PAUSE",
            "ord_index": None,
            "type": "number",
            "val": "5000",
            "version": "notdefined"
        },
        {
            "comment": "max track played in a loop in continuos mode (no motion sensor)",
            "group": "Loop",
            "id": "31798",
            "name": "LOOP_COUNT_CONT",
            "ord_index": None,
            "type": "number",
            "val": "1",
            "version": "notdefined"
        },
        {
            "comment": "min pause between 2 succesive detection. Above this, the second detection will be c",
            "group": "Detection",
            "id": "31795",
            "name": "DIFFERENCE",
            "ord_index": None,
            "type": "number",
            "val": "5000",
            "version": "notdefined"
        },
        {
            "comment": "If true the motor will stay ON between actions(not recommended)",
            "group": "Motor",
            "id": "31789",
            "name": "KEEP_MOTOR_ON",
            "ord_index": None,
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "",
            "group": "Movement",
            "id": "31786",
            "name": "ACCELERATION",
            "ord_index": None,
            "type": "number",
            "val": "12000",
            "version": "notdefined"
        },
        {
            "comment": "How fast the product moves",
            "group": "Movement",
            "id": "31785",
            "name": "SPEED",
            "ord_index": "1",
            "type": "number",
            "val": "12000",
            "version": "notdefined"
        },
        {
            "comment": "How much the product goes forward during the activation",
            "group": "Movement",
            "id": "31788",
            "name": "RANGE_MAX_POSITION",
            "ord_index": "2",
            "type": "number",
            "val": "4500",
            "version": "notdefined"
        },
        {
            "comment": "How much the product retracts during the activation",
            "group": "Movement",
            "id": "31790",
            "name": "SWING_MAX_RETRACT",
            "ord_index": "3",
            "type": "number",
            "val": "1200",
            "version": "notdefined"
        },
        {
            "comment": "If OFF the product will go into the maximum position and stay there until the audio",
            "group": "Movement",
            "id": "31787",
            "name": "CONTINUOS_MOVE",
            "ord_index": "4",
            "type": "bool",
            "val": "true",
            "version": "notdefined"
        },
        {
            "comment": "If ON the product will start to move after the audio and light",
            "group": "Movement",
            "id": "31806",
            "name": "USE_DELAY",
            "ord_index": "5",
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "The delay after which the movement will start",
            "group": "Movement",
            "id": "31807",
            "name": "DELAY_INTERVAL",
            "ord_index": "6",
            "type": "number",
            "val": "0",
            "version": "notdefined"
        },
        {
            "comment": "Product will go back slower at the end of each activation",
            "group": "Movement",
            "id": "31797",
            "name": "FRAGILE_PRODUCT",
            "ord_index": "7",
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "If OFF the product will move at specific intervals, regardless of the motion detect",
            "group": "Detection",
            "id": "31784",
            "name": "USE_MOTION_SENSOR",
            "ord_index": "8",
            "type": "bool",
            "val": "true",
            "version": "notdefined"
        },
        {
            "comment": "Pause between activations",
            "group": "Detection",
            "id": "31792",
            "name": "WAITING_TIME",
            "ord_index": "9",
            "type": "number",
            "val": "3000",
            "version": "notdefined"
        },
        {
            "comment": "Pause before messages start again",
            "group": "Detection",
            "id": "31794",
            "name": "WAITING_TIME_DETECT",
            "ord_index": "10",
            "type": "number",
            "val": "5000",
            "version": "notdefined"
        },
        {
            "comment": "The number of times the light is on during an activation. Set to 1 to keep the ligh",
            "group": "LED",
            "id": "31803",
            "name": "BLITZ_COUNT",
            "ord_index": "10",
            "type": "number",
            "val": "1",
            "version": "notdefined"
        },
        {
            "comment": "Time between flashes. Set to 0 to keep the light on",
            "group": "LED",
            "id": "31802",
            "name": "BLITZ_DELAY",
            "ord_index": "11",
            "type": "number",
            "val": "0",
            "version": "notdefined"
        },
        {
            "comment": "Light ON/OFF",
            "group": "LED",
            "id": "31801",
            "name": "USE_BLITZ",
            "ord_index": "12",
            "type": "bool",
            "val": "true",
            "version": "notdefined"
        },
        {
            "comment": "Light delay",
            "group": "LED",
            "id": "31813",
            "name": "BLITZ_START",
            "ord_index": "13",
            "type": "number",
            "val": "0",
            "version": "notdefined"
        },
        {
            "comment": "",
            "id": 999,
            "name": "USE_CLOCK_RESET",
            "type": "bool",
            "val": "false",
            "version": "notdefined"
        },
        {
            "comment": "",
            "id": 998,
            "name": "CLOCK_START_TIME",
            "type": "date",
            "val": "2018-01-01T05:00:00.000Z",
            "version": "notdefined"
        },
        {
            "comment": "",
            "id": 999,
            "name": "CLOCK_END_TIME",
            "type": "date",
            "val": "2018-01-01T21:00:00.000Z",
            "version": "notdefined"
        },
        {
            "comment": "",
            "id": 997,
            "name": "DIALOG_ROLE",
            "type": "char",
            "val": "MASTER",
            "version": "notdefined"
        },
        {
            "comment": "",
            "id": 996,
            "name": "MASTER_ID",
            "type": "char",
            "val": "0",
            "version": "notdefined"
        }]

    # Connection constants
    WIFI = "WIFI"
    GPRS = "GPRS"
    NOT_CONNECTED = "NOTCONN"

   