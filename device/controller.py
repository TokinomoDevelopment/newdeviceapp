import threading
from collections import OrderedDict
from timeit import default_timer as timer
from subprocess import Popen, PIPE
import serial
from time import sleep
from datetime import datetime
import os
from dateutil import tz as tzz

from rtclogger import RTCLogger
from .player import Player
from .arduino import Arduino
from .board import Board
from status import Status
from usagelogger import UsageLogger
from devicelogger import DeviceLogger
from const import Constants

class DeviceController(object):

    def __init__(self):
        self.is_playing = False
        self.running_general_monitor = False
        self.waiting_time = None  # campaign waiting time
        self.traffic_timer = 0 # count time until traffic timer reset
        self.sensor_timer = 0 # store timer for sensor
        self.temp_checker = False # check temperature every 10 seconds
        self.standby_checker = False # check standby every 30 seconds
        self.fan_command = None # save fan command state to send
        self.sensor_checker = False # make True to test sensor
        self.sensor_status = False # keep sensor status
        self.battery_checker = True # check battery every 5 minutes
        self.dialogue_index = 0 # keep current dialogue track index
        self.total_delay = 0 # set delay for current track in dialogue
        self.delay_timer = 0 # store timer for delay between dialogue messages
        self.send_dialog_msg = False # set to True if we need to send message to paired device
        self.observers = [] # store master->slave, slave->master callbacks for dialog mode
        self.sensor_stopped = False 
        DeviceLogger.log('info', 'Controller', 'Initialised Controller')

    def initialize(self):
        self.set_waiting_time()
        self.player = Player()
        self.arduino = Arduino()
        self.arduino.connect()
        self.arduino.start()
        self.arduino.run()
        self.monitor()
        self.set_device_connection(8)  
        self.check_mechanism_status()
        if not Status.get_setting('USE_MOTION_SENSOR'):
            self.set_sensor_stopped(True)
        self.check_battery()
        self.enable_wifi()

    def monitor(self):
        self.general_monitor_thread = threading.Thread(target=self.general_monitor, name = 'Arduino Monitor')
        self.general_monitor_thread.start()
        self.running_general_monitor = True 

    def general_monitor(self):
        # used to monitor arduino responses and recurrent activities
        t = threading.currentThread()
        while getattr(t, "running_general_monitor", True):
            sleep(0.1)
            current_time = int(timer())
            # check battery every 5 minutes 
            if current_time % 300 + 1 == 4 and not self.battery_checker:
                # restore checker to True if 5 minutes mark is passed
                # 4 value is chosen arbitrarily to be further from % 5 minutes mark
                self.battery_checker = True
            if current_time % 300 == 0 and self.battery_checker:
                self.check_battery()
                self.battery_checker = False
            # check every 30 seconds if device needs to be on standby if uses clock setting
            # don't change state if mobile app connected 
            if (Status.get_setting('USE_CLOCK_RESET') and not Status.get_mobile_connected()) or\
                (Status.get_setting('USE_CLOCK_RESET') and Status.get_dialogue_connected()):
                if current_time % 30 + 1 == 4 and not self.standby_checker:
                    # restore checker to True if 30 seconds mark is passed
                    # 4 value is chosen arbitrarily to be further from % 30 mark
                    self.standby_checker = True
                if current_time % 30 == 0 and self.standby_checker:
                    if Status.check_standby_status():
                        if Status.get_device_on() == True:
                            DeviceLogger.log('info', 'Controller', 'Clock Reset: Device OFF')
                            Status.set_device_on(False)
                    else:
                        if (Status.get_device_on() == False and not Status.get_setting('USE_DIALOG')) or\
                            (Status.get_device_on() == False and Status.get_dialogue_connected() and\
                            Status.get_setting('DIALOG_ROLE') == 'MASTER'):
                            DeviceLogger.log('info', 'Controller', 'Clock Reset: Device ON')
                            Status.set_device_on(True)
                    self.standby_checker = False
            # check alive state every 5 seconds and adjust fan speed accordingly
            # log to file to monitor device alive
            # resend led connection to arduino
            if current_time % 10 + 1 == 4 and not self.temp_checker:
                # restore checker to True if 10 seconds mark is passed
                # 4 value is chosen arbitrarily to be further from % 10 mark
                self.temp_checker = True
            if current_time % 10 == 0 and self.temp_checker:
                temperature = Status.get_cpu_temp()
                self.toggle_fan(temperature)
                self.temp_checker = False
                Status.heartbeat()
            # check sensor from mobile app
            if self.sensor_checker and not self.arduino.command_read:
                command = self.arduino.command
                self.arduino.command_read = True
                if command == '1,move;':
                    self.sensor_status = True
            # check if device in dialogue needs to send message to paired device
            if self.delay_timer > 0:
                if current_time - self.delay_timer >= self.total_delay :
                    DeviceLogger.log('info', 'Controller', 'DIALOGUE timer: {:.2f} elapsed'.format(timer() - self.delay_timer))
                    self.delay_timer = 0
                    self.set_send_dialog_msg(True)
            # send command every waiting time if device on and motion sensor is not used
            if Status.get_device_on() and not Status.get_setting('USE_MOTION_SENSOR') and self.is_playing == False:
                if 'start_no_sensor' in locals():
                    if current_time - start_no_sensor >= self.waiting_time / 1000:
                        del start_no_sensor
                        self.add_arduino_commands(['1','restart after timer'])
                        DeviceLogger.log('info', 'Controller', 'Started device after {0} ms timer'.format(str(int(self.waiting_time))))
                        # force self.is_playing = True, if not forced timer restarts before playing audio
                        self.is_playing = True
                else:
                    start_no_sensor = current_time
            # send command   
            if not self.arduino.command_read:
                command = self.arduino.command
                self.arduino.command_read = True
                args = command.replace(';', '').split(',')
                self.add_arduino_commands(args)

    def add_arduino_commands(self, args):
        try:
            current_time = timer()
            int_arg = int(args[0])
            # enable sending next move command from arduino
            if (int_arg == 0 and args[1] == 'done') or (int_arg == 3 and args[1] == 'Done'): 
                # this is due to arduino response: 
                # 0,done; if command executed or 3,Done; if stepper home
                self.arduino.run_move_commands = True
            # activate device if device on and sensor is not used 
            if Status.get_device_on() and int_arg == 1 and args[1] == 'restart after timer':
                self.arduino.run_move_commands = True
                self.traffic_timer = current_time
                self.activate_device()  
                Status.increment_activations()
                Status.save_usage()
                UsageLogger.log_activations('info', 'Usage', 'Activation recorded')
                DeviceLogger.log('info', 'Usage', 'Activation recorded')
            # activate device if device on and timer is above waiting time and motion sensor is used
            # if dialogue is used activate only if dialog index = 0 and delay_timer = 0
            elif Status.get_device_on() and (int_arg == 1 and Status.get_setting('USE_MOTION_SENSOR') and \
            self.is_playing == False and current_time - self.sensor_timer >= self.waiting_time / 1000) and \
            self.dialogue_index == 0 and self.delay_timer == 0:
                self.activate_device()
                Status.increment_activations()
                Status.increment_traffic()
                UsageLogger.log_activations('info', 'Usage', 'Activation recorded')
                DeviceLogger.log('info', 'Usage', 'Activation recorded')
                UsageLogger.log_trafic('info', 'Traffic', 'Traffic recorded')
                DeviceLogger.log('info', 'Traffic', 'Traffic recorded')
                Status.save_usage()
                self.traffic_timer = current_time
            # check if we record sensor detection as traffic
            # works for sensor campaigns and non sensor campaigns
            elif Status.get_device_on() and int_arg == 1:
                if current_time - self.traffic_timer >= Constants.traffic_timer_limit:
                    self.traffic_timer = current_time
                    UsageLogger.log_trafic('info', 'Traffic', 'Traffic recorded')
                    DeviceLogger.log('info', 'Traffic', 'Traffic recorded')
                    Status.increment_traffic()
                    Status.save_usage()
            # check mechanism status response
            if int_arg == 14 and args[1] == '1':
                Status.set_mechanism_status('OK')
            elif int_arg == 14 and args[1] != '1':
                Status.set_mechanism_status('Damaged')
            # check battery status:
            if int_arg == 11:
                Status.set_battery_status(args[1],args[2])
        except ValueError:
            DeviceLogger.log('error', 'Controller', 'Cannot read arduino response; first character cannot be converted to int')
 
    def activate_device(self):
        # activate device    
        # CONTINOUS_MOVE = True -> extend
        # movement = 0 -> pattern    
        # movement = 2 -> random 
        # movement = 3 -> bounce
        # movement = 4 -> bounce
        if Status.get_setting('USE_DIALOG'):
            # get next file in file list to be played
            # set delay for sending message to paired device
            chosen_list = Status.get_dialogue_list()[Status.get_list_index()]
            chosen_delays_list = Status.get_delays_list()[Status.get_list_index()]
            idx = chosen_list[self.dialogue_index]
            delay = chosen_delays_list[self.dialogue_index]
            next_file, movement, pattern, duration = Status.get_file_by_id(idx)
            self.total_delay = duration + delay 
            DeviceLogger.log('info', 'Controller', '{0} duration {1}, delay duration {2}'.format(next_file, duration, delay))
            DeviceLogger.log('info', 'Controller', 'Delay for {0} set to {1}'.format(next_file, self.total_delay))
            if self.total_delay < 0:
                self.total_delay = 3
            self.delay_timer = timer()
        else:
            # get next file based on id index
            next_file, movement, pattern = Status.get_next_file_data()
        self.activate_light()
        self.play_file(next_file)
        if not Status.get_setting('CONTINUOS_MOVE'):
            self.extend()
            DeviceLogger.log('info', 'Controller', 'Device activated: extend with no continuous move')
        elif movement == 0:
            self.pattern_move(pattern, 0)
        elif movement == 2:
            self.extend_random()
        elif movement == 3:
            self.extend_bounce()
        elif movement == 4:  
            self.pattern_move(pattern, 4)

    def pattern_move(self, pattern, movement):
        if movement == 0:
            DeviceLogger.log('info', 'Controller', 'Device activated: patterns')
        if movement == 4:
            DeviceLogger.log('info', 'Controller', 'Device activated: auto-generated')
        temp = {}
        for p in pattern: 
            key = p['id']
            temp[key] = [int(p['distance']), int(p['pause']), int(p['speed'])]
        pattern_list = list(OrderedDict(sorted(temp.items())).values())
        fragile = int(Status.get_setting("FRAGILE_PRODUCT"))
        for p in pattern_list:
            pattern_command = [19, p[0], p[2], fragile]
            self.arduino.pattern_commands.append(pattern_command)
            self.arduino.pattern_commands_wait.append(p[1])
        self.arduino.pattern_commands.append([20, 'home'])
        self.arduino.pattern_commands_wait.append(50)
        self.arduino.run_pattern_commands = True
        self.arduino.run_move_commands = False
        
    def extend_random(self):
        # extend and bounce random command
        DeviceLogger.log('info', 'Controller', 'Device activated: random')
        self.extend()
        self.random()

    def extend_bounce(self):
        # extend and bounce command
        DeviceLogger.log('info', 'Controller', 'Device activated: bounce')
        self.extend()
        self.bounce()

    def extend(self):
        # extend command: Id 2
        steps = Status.get_setting('RANGE_MAX_POSITION')
        speed = Status.get_setting('SPEED')
        accel = Status.get_setting('ACCELERATION')
        delay = 0
        if Status.get_setting('USE_DELAY') and Status.get_setting('DELAY_INTERVAL') > 0:
            delay = Status.get_setting('DELAY_INTERVAL')
        slow = int(Status.get_setting('SLOW_START'))
        move_command = [2, 1, steps, speed, accel, accel, delay, slow]
        self.arduino.move_commands.append(move_command)

    def move_home(self):
        # home (for random or bounce) command: Id 3
        fragile = Status.get_setting('FRAGILE_PRODUCT')
        speed = 6000
        accel = 10000
        if fragile:
            speed = 4000
            accel = 4000
        self.arduino.move_commands = []
        home_command = [3, speed, accel, accel]
        self.arduino.move_commands.append(home_command) 
        self.arduino.run_move_commands = True  
        DeviceLogger.log('info', 'Controller', 'Device moved to home position from move') 

    def pattern_home(self):
        # home (for pattern) command: Id 20
        self.arduino.pattern_commands = []
        self.arduino.pattern_commands_wait = []
        self.arduino.pattern_commands.append([20, 'home'])
        self.arduino.pattern_commands_wait.append(50)
        self.arduino.run_pattern_commands = True
        DeviceLogger.log('info', 'Controller', 'Device moved to home position from pattern') 

    def bounce(self):
        # bounce command: Id 4
        speed = Status.get_setting('SPEED')
        accel = Status.get_setting('ACCELERATION')
        steps = Status.get_setting('SWING_MAX_RETRACT')
        bounce_command = [4, speed, accel, accel, steps]
        self.arduino.move_commands.append(bounce_command) 

    def random(self, *args):
        # bounce random command: Id 5
        speed = Status.get_setting('SPEED')
        accel = Status.get_setting('ACCELERATION')
        steps = Status.get_setting('RANGE_MAX_POSITION')
        swing = Status.get_setting('SWING_MAX_RETRACT')
        random_command = [5, speed, accel, accel, steps, swing]
        self.arduino.move_commands.append(random_command) 
                
    def toggle_light(self, state):
        # set light on/off: Id 8 (state = 0 or 1)
        light_command = (8, state)
        self.arduino.generic_commands.append(light_command)

    def activate_light(self):
        if Status.get_setting('USE_BLITZ'):
            self.arduino.use_light = True

    def deactivate_light(self):
        self.arduino.use_light = False
        self.arduino.light_state = -1
        self.arduino.start_light = 0
        self.arduino.light_command_send = True
        self.toggle_light(0)

    def set_sensor_checker(self, value):
        # set to True to check sensor
        self.sensor_checker = value 


    # Player

    def play_file(self, next_file):
        Board.sound_on()
        DeviceLogger.log('info', 'Controller', 'Board sound on')
        self.is_playing = True
        self.player.play(next_file, self.player_done)  
        
    def player_done(self, file):
        self.is_playing = False
        self.sensor_timer = timer()
        self.move_home()
        self.deactivate_light()
        DeviceLogger.log('info', 'Controller', 'Stopped playing {0} file'.format(file))
        Board.sound_off()
        DeviceLogger.log('info', 'Controller', 'Board sound off')


    def play_test_file(self, filename):
        Board.sound_on()
        DeviceLogger.log('info', 'Controller', 'Board sound on')
        sleep(0.1)
        self.player.play(filename, self.stop_test_file_play)

    def stop_test_file_play(self, file = None):
        # stop playing file from mobile app
        self.player.stop()
        if file != None:
            DeviceLogger.log('info', 'Controller', 'Stopped playing {0} file'.format(file))
        Board.sound_off()
        DeviceLogger.log('info', 'Controller', 'Board sound off')

    # Setters

    def set_args_arduino(self):
        # set True to monitor arduino messages
        self.arduino.set_args_arduino()
   
    def set_sensor_stopped(self, value):
        # set flag to stop sensor 
        self.sensor_stopped = value
        if value == True:
            self.arduino.run_move_commands = False
            DeviceLogger.log('info', 'Controller', 'Sensor OFF')
        elif value == False:
            self.arduino.run_move_commands = True
            DeviceLogger.log('info', 'Controller', 'Sensor ON')

    def bind_to(self, callback):
        self.observers.append(callback)

    def set_send_dialog_msg(self, value):
        self.send_dialog_msg = value  
        if self.send_dialog_msg == True:
            for callback in self.observers:
                callback()
 
    def set_device_connection(self, state):
        # exhaustive codes, combination of cloud connection and bluetooth connection
        # 0 = not connected
        # 1 = connected through WiFi
        # 2 = connected through GPRS
        # 3 = connected through mobile app
        # 4 = dialogue campaign and no internet
        # 5 = dialogue campaign and WiFi
        # 6 = dialogue campaign and GPRS
        # 7 = connected through mobile app and WiFi
        # 8 = connected through mobile app and GPRS
        # -------------------------
        # codes for cloud connection to be evaluated along with bluetooth connection
        # 8 = not connected
        # 9 = set connected through WiFi
        # 10 = set connected through GPRS
        # command format: 9, r, g, b, state, interval
        if state == 8 and not Status.get_dialogue_connected() and not Status.get_mobile_connected():
            rgb_commnand = [9, 0, 0, 255, 1, 0]
            Status.set_device_connection(0)
        elif state == 9 and not Status.get_dialogue_connected() and not Status.get_mobile_connected():
            rgb_commnand = [9, 0, 255, 0, 1, 0]
            Status.set_device_connection(1)   
        elif state == 10 and not Status.get_dialogue_connected() and not Status.get_mobile_connected():
            rgb_commnand = [9, 0, 255, 0, 1, 1000]
            Status.set_device_connection(2)
        elif state == 8 and Status.get_mobile_connected() and not Status.get_dialogue_connected():
            rgb_commnand = [9, 0, 0, 255, 1, 1000]
            Status.set_device_connection(3)
        elif state == 9 and Status.get_mobile_connected() and not Status.get_dialogue_connected():
            rgb_commnand = [9, 0, 0, 255, 1, 1000]
            Status.set_device_connection(7)
        elif state == 10 and Status.get_mobile_connected() and not Status.get_dialogue_connected():
            rgb_commnand = [9, 0, 0, 255, 1, 1000]
            Status.set_device_connection(8)
        elif state == 8 and Status.get_dialogue_connected():
            rgb_commnand = [9, 75, 0, 130, 1, 0]
            Status.set_device_connection(4)
        elif state == 9 and Status.get_dialogue_connected():
            rgb_commnand = [9, 100, 100, 100, 1, 0]
            Status.set_device_connection(5)
        elif state == 10 and Status.get_dialogue_connected():
            rgb_commnand = [9, 100, 100, 100, 1, 1000]
            Status.set_device_connection(6)
        self.arduino.generic_commands.append(rgb_commnand)

    def toggle_fan(self,cputemp):
        # set fan speed based on cpu temperature: Id 13
        if cputemp < 40:
            state = 0
            speed = 0
        elif cputemp > 42:
            state = 1
            speed = 255
        else:
            state = 1
            speed = int(cputemp*255/60)
        new_fan_command = [13, state, speed]
        if self.fan_command != new_fan_command:
            self.fan_command = new_fan_command
            # Don't log this anymore, it clogs the log file 
            #DeviceLogger.log('info', 'Controller', 'Cpu temperature is {2} degreees, fan state set to {0} and {1} speed'.format(state, speed, cputemp))
            self.arduino.generic_commands.append(self.fan_command) 

    def test_fan(self,state):
        # set fan speed based on cpu temperature: Id 13
        # used for bluetooth connection
        if state == 1:
            speed = 255
        elif state == 0:
            speed = 0
        fan_command = [13, state, speed]            
        self.arduino.generic_commands.append(fan_command) 

    def set_sensor_status(self, value):
        # turn sensor on/off
        self.sensor_status = value

    def set_dialogue_index(self, value):
        self.dialogue_index = value
        DeviceLogger.log('info', 'Controller', 'Dialogue index value set to ' + str(self.dialogue_index))

    # Checkers

    def check_mechanism_status(self):
        # bounce random command: Id 14
        check_mechanism_command = [14]
        self.arduino.generic_commands.append(check_mechanism_command)          

    def check_battery(self):
        # check battery command: Id 11
        check_mechanism_command = [11]
        self.arduino.generic_commands.append(check_mechanism_command) 

    # Getters 

    def get_sensor_stopped(self):
        return self.sensor_stopped

    def get_is_playing(self):
        return self.is_playing

    def get_datetime(self):
        dt = Board.read_rtc_date() # datetime.datetime.now()
        return '{0}:{1}:{2}'.format(dt.hour,dt.minute,dt.second)

    def get_sensor_status(self):
        return self.sensor_status

    def get_dialogue_index(self):
        return self.dialogue_index

    def get_send_dialog_msg(self):
        return self.send_dialog_msg

    def get_observers(self):
        return self.observers

    def get_sensor_checker(self):
        return self.sensor_checker

    # Others

    def stop_device(self):
        self.player.stop()
        self.pattern_home()
        self.move_home()
        
    def set_waiting_time(self):
        # set waiting time if motion sensor is not used
        self.waiting_time = Status.get_setting('WAITING_TIME')
        if self.waiting_time == False:
            self.waiting_time = 100
            DeviceLogger.log('error', 'Controller', 'Waiting time setting not found; set to default 100 ms value')
        else:
            if self.waiting_time == 0:
                self.waiting_time = 100
            DeviceLogger.log('info', 'Controller', 'Waiting time set to {0} ms'.format(int(self.waiting_time)))

    # Modem info methods

    def connect_to_modem(self, baudrate):
        # connect to modem with speed parameter
        self.port = serial.Serial()
        self.port.port = '/dev/ttyS1'
        self.port.baudrate = baudrate
        self.port.bytesize = 8
        self.port.parity = 'N'
        self.port.stopbits = 1 
        self.port.timeout = 1
        self.port.rtscts = False
        self.port.dsrdtr = False
        self.port.open()       
        self.port.flushInput()
        self.port.flushOutput()

    def write_command(self, cmd, timeout = 2):
        # write command function wrapper for better accuracy of data capture
        self.port.write(cmd.encode())
        answer = ""
        read_timeout = timeout
        quantity = self.port.in_waiting
        while True:
            if quantity > 0:
                answer += str(self.port.read(quantity))
                return answer
            else:
                sleep(read_timeout)
            quantity = self.port.in_waiting
            if quantity == 0:
                break

    def set_modem_type(self):
        # get modem type
        try:
            p = Popen(['sudo poff -a'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            connected,err = p.communicate() 
            if err:
                DeviceLogger.log('info', 'Controller', 'Error trying to stop pppd')
            elif connected == b'':
                DeviceLogger.log('info', 'Controller', 'All pppd connections stopped')
            else:
                DeviceLogger.log('info', 'Controller', 'No pppd is running; none stopped')             
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to stop pppd: ' + str(ex))

        DeviceLogger.log('info', 'Controller', 'Getting modem information')
        sleep(3) # sleep in order to let serial port be ready
        # Get modem type
        try:
            self.connect_to_modem(115200)  
            at_response = self.write_command('AT\r')
            at_response = self.write_command('ATI\r',1)
            if 'SIM800' in str(at_response):
                Status.set_modem('SIM800')
            if 'SIM7600' in str(at_response):
                Status.set_modem('SIM7600')
            DeviceLogger.log('info', 'Controller', 'Detected modem: ' + str(Status.get_modem()))
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to get modem type: ' + str(ex))

    def set_modem_signal(self):
        # Get signal
        try:
            at_response = self.write_command('AT+CSQ\r',1)
            csq_value = str(at_response).split(' ')[1].split('\\r')[0].split(',')[0]
            modem_signal = self.evaluate_signal(int(csq_value))
            Status.set_modem_signal(modem_signal)
            DeviceLogger.log('info', 'Controller', 'Modem signal: ' + str(Status.get_modem_signal()))
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to get modem signal: ' + str(ex))

    def set_sim_id(self):
        # Get SIM ID
        try:
            if Status.get_modem() == 'SIM800':
                at_response = self.write_command('AT+CCID\r',1)
                sim_id = str(at_response).split('f')[0].split('n')[1]
            if Status.get_modem() == 'SIM7600':
                at_response = self.write_command('AT+CCID\r',1)
                sim_id = str(at_response).split(' ')[1].split('\\r')[0]
            if sim_id[:5] != 'ERROR':
                Status.set_sim_id(sim_id)
            else:
                Status.set_sim_id(-1)
            DeviceLogger.log('info', 'Controller', 'Modem SIM ID: ' + str(Status.get_sim_id()))
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to get SIM ID: ' + str(ex))
            Status.set_sim_id(-1)

    def set_imei(self):
        # set information about modem IMEI
        try:
            at_response = self.write_command('AT+CGSN\r',1)
            imei = str(at_response).split('\\n')[1].split('\\r')[0]
            Status.set_imei(imei)
            DeviceLogger.log('info', 'Controller', 'Modem IMEI: ' + str(Status.get_imei()))
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to get IMEI: ' + str(ex))
 
    def set_modem_firmware(self):
        try:
            if Status.get_modem() == 'SIM800':
                at_response = self.write_command('AT+CGMR\r',1)
                firmware = str(at_response).split('\\n')[1].split('\\r')[0]
                Status.set_modem_firmware(firmware)
            if Status.get_modem() == 'SIM7600':
                at_response = self.write_command('AT+CGMR\r')
                firmware = str(at_response).split(' ')[1].split('\\r')[0] 
                Status.set_modem_firmware(firmware)
            DeviceLogger.log('info', 'Controller', 'Modem firmware: ' + str(Status.get_modem_firmware()))
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to get modem firmware: ' + str(ex))

    def set_gprs_provider(self):
        try:
            at_response = self.write_command('AT\r')
            at_response = self.write_command('AT+COPS?\r',5)
            provider = str(at_response).split('"')[1]
            Status.set_provider(provider)
            DeviceLogger.log('info', 'Controller', 'GPRS provider: ' + str(Status.get_provider()))
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to connect to GPRS provider: ' + str(ex))
            DeviceLogger.log('error', 'Controller', str(at_response))

    def set_device_location_sim800(self):
        try:
            at_response = self.write_command('AT+CGATT=1\r',3)
            at_response = self.write_command('"AT+SAPBR=3,1,"CONTYPE","GPRS"\r"',3)
            at_response = self.write_command('AT+SAPBR=3,1,"APN","{0}"\r'.format(Status.get_iot_provider()),3)
            at_response = self.write_command('AT\r')
            at_response = self.write_command('AT+SAPBR=1,1\r',3)
            at_response = self.write_command('AT\r')
            at_response = self.write_command('AT+SAPBR=2,1\r',3)
            at_response = self.write_command('AT\r')
            at_response = self.write_command('AT+CLBS=1,1\r',5)
            location_raw = str(at_response).split(',')
            location = "['{0}','{1}']".format(location_raw[3],location_raw[2])
            Status.set_device_location(location)
            DeviceLogger.log('info', 'Controller', 'Device location: ' + str(Status.get_device_location()))
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to get location: ' + str(ex))
            DeviceLogger.log('error', 'Controller', str(at_response))
        finally:
            cell_datetime = self.write_command('AT+CIPGSMLOC=1,1\r',5)
            self.check_datetime(cell_datetime)
            at_response = self.write_command('AT\r')
            at_response = self.write_command('AT+CGATT=0\r')
            at_response = self.write_command('AT+CGATT=0\r')

    def check_datetime(self, cell_datetime):
        try:
            dt_array = cell_datetime.split(',')
            date = dt_array[4]
            time = dt_array[5].split('\\')[0]
            cell_date = datetime.strptime(date + ' ' + time,'%Y/%m/%d %H:%M:%S')
            utc_offset = datetime.now(tzz.gettz(Status.get_timezone())).utcoffset() 
            cell_date += utc_offset
            board_date = Board.read_rtc_date()
            if abs(cell_date - board_date).total_seconds() > 60:
                RTCLogger.log('error', 'Status', 'Board time error: ' + str(board_date) + '; set date to ' + str(cell_date) + ' from GPRS date')
                DeviceLogger.log('error', 'Status', 'Board time error: ' + str(board_date) + '; set date to ' + str(cell_date) + ' from GPRS date')
                Status.set_date_to_board(cell_date)
                Status.set_date_from_board()
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Cannot check datetime: ' + str(ex))
      
    def set_device_location_sim7600(self):
        try:
            at_response = self.write_command('AT+CGDCONT=1,"IP","{0}"\r'.format(Status.get_iot_provider()),4)
            at_response = self.write_command('AT+CSOCKSETPN=1\r',4)
            at_response = self.write_command('AT+CNETSTART\r',4)
            at_response = self.write_command('AT+CNETIPADDR?\r',4)
            at_response = self.write_command('AT+CLBS=1\r',4)
            location_raw = str(at_response).split(',')
            if (int(float(location_raw[1])) == 0 and int(float(location_raw[2])) == 0):
                DeviceLogger.log('error', 'Controller', 'Location incorrect: [0, 0]')
                DeviceLogger.log('error', 'Controller', str(at_response))
            else:
                location = "['{0}','{1}']".format(location_raw[1],location_raw[2])
                Status.set_device_location(location)
                DeviceLogger.log('info', 'Controller', 'Device location: ' + str(Status.get_device_location()))
        except Exception as ex:
            DeviceLogger.log('error', 'Controller', 'Error trying to get location: ' + str(ex))
            DeviceLogger.log('error', 'Controller', str(at_response))
        finally:
            at_response = self.write_command('AT\r')
            at_response = self.write_command('AT+CNETSTOP=1\r')
 
    def set_device_location(self):
        if Status.get_modem() == 'SIM800':
            self.set_device_location_sim800()       
            if Status.get_device_location() == None:
                self.set_device_location_sim800()  
            if Status.get_device_location() == None:
                self.set_device_location_sim800()
            if Status.get_device_location() != None:
                Status.set_force_location(1)
        if Status.get_modem() == 'SIM7600':
            self.set_device_location_sim7600()  
            if Status.get_device_location() == None:
                at_response = self.write_command('AT+CPOF')    
                sleep(30)    
                self.set_device_location_sim7600()   
            if Status.get_device_location() == None:
                self.set_device_location_sim7600()   

    def set_mac(self):
        try:
            mac = os.popen('nmcli device show wlan0 | grep -i hw').readline().strip()[-17:]
            Status.set_mac(mac)
            DeviceLogger.log('info', 'Controller', 'WiFi card mac address: ' + str(Status.get_mac()))
        except Exception as ex:
            DeviceLogger.log('info', 'Controller', 'Cannot detect WiFi card mac address: ' + str(ex))

    def set_nanopi_serial(self):
        try:
            proc = Popen(['cat /proc/cpuinfo | grep Serial'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            out,err = proc.communicate()
            nanopi_serial = out.decode("utf-8").split(' ')[1][:-1]
            Status.set_nanopi_serial(nanopi_serial)
            DeviceLogger.log('info', 'Controller', 'NanoPi serial: ' + str(Status.get_nanopi_serial()))
        except Exception as ex:
            DeviceLogger.log('info', 'Controller', 'Cannot detect nanopi serial: ' + str(ex))

    def set_device_info(self):
        #set information about modem name, SIM ID, signal strength, modem IMEI, wifi card mac
        self.set_modem_type()
        self.set_modem_signal()
        self.set_sim_id()
        self.set_imei()
        self.set_modem_firmware()
        self.set_mac()
        self.set_nanopi_serial()
        if Status.get_sim_id() != -1: # Cloud set to No Sim if id == -1
            self.set_gprs_provider()

    def set_device_location_datetime(self):
        if Status.get_sim_id() != -1: 
            self.set_device_location()
        self.port.close()

    def evaluate_signal(self, msig):
        if int(msig) >= 20:
            return 'Excellent'
        elif int(msig) >=15 and int(msig) < 20:
            return 'Good'
        elif int(msig) >=10 and int(msig) < 15:
            return 'OK'
        elif int(msig) < 10:
            return 'Weak'

    def enable_wifi(self):
        # enable device wifi
        Popen(['sudo nmcli radio all on'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
        sleep(0.5)
        DeviceLogger.log('info', 'Controller', 'WiFi enabled')
        self.check_tokinomo_network()

    def check_tokinomo_network(self):
        try:
            p = Popen(['iwgetid -r'],stdin=PIPE, stdout=PIPE, shell=True)
            connection, err = p.communicate()
            if connection:
                DeviceLogger.log('info', 'Controller', 'WiFi already connected to: {0}'.format(connection.decode("utf-8").rstrip()))
            else:
                p = Popen(['sudo iw dev wlan0 scan | grep SSID: | cut -d " " -f 2'],stdin=PIPE, stdout=PIPE, shell=True)
                networks, err = p.communicate()
                out = networks.decode("utf-8").split(' ')[0].split('\n')
                if 'tokinomo' in out:
                    s = Popen(['sudo nmcli c delete tokinomo'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                    deleted,err = s.communicate()
                    p = Popen(['sudo nmcli device wifi connect tokinomo password altaintrebare'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
                    conn,err = p.communicate()
                    if err:
                        DeviceLogger.log('error', 'Controller', 'Cannot connect to tokinomo WiFi network')
                    else:
                        DeviceLogger.log('info', 'Controller', 'Connection to tokinomo WiFi network established')
        except Exception as ex:
            DeviceLogger.log('info', 'Controller', 'Cannot check for tokinomo network: ' + str(ex))

    def disable_wifi(self):
        Popen(['sudo nmcli radio all off'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
        DeviceLogger.log('info', 'Controller', 'WiFi disabled')

    def disable_modem(self):
        try:
            p = Popen(['sudo poff -a'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            connected,err = p.communicate() 
            if err:
                DeviceLogger.log('info', 'NetworkManager', 'Error trying to stop pppd')
            elif connected == b'':
                DeviceLogger.log('info', 'NetworkManager', 'All pppd connections stopped')
            else:
                DeviceLogger.log('info', 'NetworkManager', 'No pppd is running; none stopped')             
        except Exception as ex:
            DeviceLogger.log('error', 'NetworkManager', 'Error trying to stop pppd: ' + str(ex))
 
    def reset_bind(self):
        self.observers = []

    def is_wlan0_connected(self, ifn):
        try:
            p = Popen(['ip route list'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            connected,err = p.communicate()  
            output = str(connected).split()
            if 'wlan0' in output:
                return True
            else:
                return False
        except Exception as ex:
            DeviceLogger.log('error', 'NetworkManager', 'Cannot check wifi connection: ' + str(ex))
            return False


DeviceController = DeviceController()

