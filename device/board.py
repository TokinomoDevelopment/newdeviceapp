import NPi.GPIO as GPIO # used for development
#import RPi.GPIO as GPIO # used for building
from .rtc import ds3231
import datetime
import time
import smbus
from .rtc import _local2utc

class Board: 
    MUTEPIN = 12
    RESETPIN = 24
    NETLIGHT = 13 #IOA2; has 2nd serial RTS2 
    bus = None
    address = 0x68

    rtc = None

    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.MUTEPIN, GPIO.OUT)
        GPIO.setup(self.RESETPIN, GPIO.OUT)
        GPIO.setup(self.NETLIGHT, GPIO.IN)
        self.rtc = ds3231(utc=False)
        self.bus = smbus.SMBus(0)
        self.reset()

    def reset(self):
        GPIO.output(self.RESETPIN, 0)
        time.sleep(0.5)
        GPIO.output(self.RESETPIN, 1)

    def sound_on(self):
        GPIO.output(self.MUTEPIN, 1)

    def sound_off(self):
        GPIO.output(self.MUTEPIN, 0)   

    def read_netlight(self):
        return GPIO.input(self.NETLIGHT) 

    def read_rtc_date(self):
        dt = self.rtc.read_datetime()
        return dt

    def read_utc_date(self):
        dt = self.rtc.read_datetime()
        return _local2utc(dt)
     
    def set_rtc_date(self, dt):
        y = dt.year - 2000
        mt = dt.month
        d = dt.day
        h = dt.hour
        m = dt.minute
        s = dt.second
        self.rtc.write_all(seconds=s, minutes=m, hours=h, day_of_week=None,
            day_of_month=d, month=mt, year=y)

    def get_temp(self):
        byte_tmsb = self.bus.read_byte_data(self.address,0x11)
        byte_tlsb = bin(self.bus.read_byte_data(self.address,0x12))[3:].zfill(8)
        temp = byte_tmsb+int(float(byte_tlsb[0]))*2**(-1)+int(float(byte_tlsb[1]))*2**(-2)
        if temp > 35:
            return 35
        else:
            return temp
 

Board = Board()


 
