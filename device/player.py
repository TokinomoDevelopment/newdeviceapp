import os
import threading
from subprocess import Popen, PIPE
from time import sleep


from devicelogger import DeviceLogger
from status import Status

class Player(object):
    def __init__(self):
        DeviceLogger.log('info', 'Player', 'Initialised Player')
    
    def play(self, audio, done):
        def playing():
            active = Popen(['play','-q', '/home/pi/device/500-milliseconds-of-silence.mp3'],stdout=PIPE,stderr=PIPE)
            a,e = active.communicate()
            DeviceLogger.log('info', 'Player', 'Playing {0} file'.format(audio))
            proc = Popen(['play', '-q', '/home/pi/device/Files/' + audio, 'bass', '-50'], stdout=PIPE, stderr=PIPE)  
            sleep(0.05)
            out, err = proc.communicate()
            if err:
                if(str(err).find("can't open input file") != -1):
                    DeviceLogger.log('error', 'Player', 'Cannot open {0} file'.format(audio))
                    done(audio)
                else:
                    done(audio)
            else:
                proc.wait()
                done(audio)

        playthread = threading.Thread(target=playing, name = 'Play Audio')
        playthread.daemon = True
        if Status.get_setting('SLOW_START'):
            volume_thread = threading.Thread(target=self.increase_volume)
            volume_thread.daemon = True
            volume_thread.start()
        playthread.start()
        
    def increase_volume(self):
        volume = Status.get_device_volume()
        for i in range(30, volume):
            os.popen('amixer sset "Line Out" {0}%'.format(volume))
            sleep(0.05)


    def stop(self):
        p = Popen('sudo killall -9 play', stdout=PIPE, stderr=PIPE, shell=True)
        out, err = p.communicate()