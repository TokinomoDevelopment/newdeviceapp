import serial
import threading
from time import sleep
from timeit import default_timer as timer
from subprocess import Popen, PIPE
import os

from devicelogger import DeviceLogger
from status import Status
from .board import Board

class Arduino():

    def __init__(self):

        self.move_commands = [] # store bounce and bounce random commands
        self.pattern_commands = [] # store pattern commands 
        self.pattern_commands_wait = [] # store wait time between pattern commands
        self.generic_commands = [] # store generic commands besides motion
        self.command = None # store valid arduino command
        self.command_read = True # store if read command boolean
        self.running_read_serial = False
        self.running_execute_thread = False
        self.port = None
        self.args_arduino = False
        self.run_move_commands = True # set to False if waiting for ok from arduino
        self.run_pattern_commands = False # set to True if need to run pattern commands
        self.light_state = -1 # -1 = not working, 1 = on, 0 = off
        self.light_command_send = True # set to False after each light command
        self.use_light = False # set to True to start light on/off pattern
        self.start_light = 0 # timestamp when light changes state
        DeviceLogger.log('info', 'Arduino', 'Initialised Arduino')

    def connect(self):
        # Open arduino port
        self.port = serial.Serial()
        self.port.port = '/dev/ttyS2'
        self.port.baudrate = 9600
        self.port.timeout = 0.2
 
        try:
            self.port.open()
            DeviceLogger.log('info', 'Arduino', 'Arduino connected')
        except Exception as ex:
            DeviceLogger.log('error', 'Arduino', 'Cannot connect arduino: ' + str(ex))
            DeviceLogger.log('error', 'Status', 'Closing app')
            os._exit(0)

    ## Main arduino thread

    def start(self):
        # start read serial thread
        if not self.port.isOpen():
            self.port.open()
        self.reading_thread = threading.Thread(target=self.read_serial, args=("task",), name = 'Arduino read serial')
        self.reading_thread.start()
        self.running_read_serial = True  

    def read_serial(self, *args):
        # start a new thread that reads serial data
        t = threading.currentThread()
        while getattr(t, "running_read_serial", True):
            sleep(0.1)
            try:
                line = self.port.readline()
                cmd = line.decode('ISO-8859-1').rstrip()
                if cmd != "":
                    DeviceLogger.log('info', 'Arduino', 'Received from arduino: ' + str(cmd))
                    self.command = cmd
                    self.command_read = False
            except serial.SerialException as ex:
                DeviceLogger.log('error', 'Arduino', 'CRITICAL: SERIAL EXCEPTION. NEED TO REBOOT')
                p = Popen(['sudo reboot'],stdin=PIPE,stdout=PIPE,stderr=PIPE,shell=True)
            except Exception as ex:
                DeviceLogger.log('error', 'Arduino', 'Cannot read arduino response: ' + str(ex))
        DeviceLogger.log('info', 'Arduino', 'Stopping arduino')

 
    ## Execute thread

    def run(self):
        # run registered commands
        self.execute_thread = threading.Thread(target=self.execute, name = 'Execute Arduino commands')
        self.running_execute_thread = True         
        self.execute_thread.start()
        pass

    def execute(self):
        t = threading.currentThread()
        while self.running_execute_thread:
            sleep(0.01)
            # run patterns
            if 'start_patterns' in locals():
                if timer()- start_patterns >= time_patterns:
                    self.run_pattern_commands = True
                    del start_patterns
            if self.run_pattern_commands:
                if len(self.pattern_commands):
                    command = self.pattern_commands.pop(0)
                    self.send_command(command)
                    start_patterns = timer() 
                    time_patterns = self.pattern_commands_wait.pop(0) / 1000
                    self.run_pattern_commands = False
            # run bounce and bounce random
            if self.run_move_commands:
                if len(self.move_commands):
                    command = self.move_commands.pop(0)
                    self.send_command(command)
                    self.run_move_commands = False

            # run light commands
            if self.use_light:
                if self.start_light > 0:
                    if timer() - self.start_light >= time_light:
                        if self.light_state == 1:
                            command = [8, 1]
                        elif self.light_state == 0:
                            command = [8, 0]
                        self.send_command(command)
                        self.start_light = 0
                        self.light_command_send = True
                if self.light_command_send:
                    self.start_light = timer()
                    self.light_command_send = False
                    if self.light_state == -1:
                        time_light = int(Status.get_setting('BLITZ_PREDELAY')) / 1000
                        self.light_state = 1
                    elif self.light_state == 1:
                        if int(Status.get_setting('BLITZ_OFF')) / 1000 != 0:
                            # don't turn off light if blitz off equals 0
                            time_light = int(Status.get_setting('BLITZ_ON')) / 1000
                            self.light_state = 0
                        else:
                            self.start_light = 0
                    elif self.light_state == 0:
                        time_light = int(Status.get_setting('BLITZ_OFF')) / 1000
                        self.light_state = 1

            # run other commands
            if len(self.generic_commands):
                command = self.generic_commands.pop(0)
                self.send_command(command)

    def start_execute(self):
        self.run()

    def stop_execute(self):
        self.running_execute_thread = False

    # Setters 

    def set_args_arduino(self):
        self.args_arduino = True

    def send_command(self, command):
        params = ",".join(['%s' % (x) for x in command[1:]])
        cmd = "{0},{1};".format(command[0], params).encode()
        self.port.write(cmd)