#!/bin/bash
sudo apt-get update
sudo apt-get install python3-setuptools -y
sudo apt-get install python3-pip  
sudo git clone https://github.com/Tungsteno74/NPi.GPIO
cd NPi.GPIO               
sudo python3 setup.py install
pip3 install smbus
pip3 install arrow==0.17.0 
pip3 install pyserial
pip3 install ntplib
pip3 install pybleno   
sudo apt-get install python3-dbus
sudo pip3 install gatt
pip3 install pexpect
pip3 install pytz
  
