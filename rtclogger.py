import os
import logging

from logger import specific_logger
from device.board import Board

class RTCLogger():

    def __init__(self):
        # initiate object with locations
        self.path = '/home/pi/device/log/rtc' 
        self.name = 'RTC'
        self.full_name = '/home/pi/device/log/rtc/rtc_log'
        if not os.path.isdir(self.path):
            os.mkdir(self.path)
        self.logger = specific_logger(self.name, self.full_name)
     
    def log(self, type, location, message):
        # log all types of messages
        date = Board.read_rtc_date()
        if type == 'info':
            self.logger.info( '{:<38}'.format('{1}:INFO [{0}]'.format(date,self.name)) + '{:<20}'.format('[{0}]'.format(location)) + message )   
        elif type == 'debug':
            self.logger.debug( '{:<38}'.format('{1}:DEBUG [{0}]'.format(date,self.name)) + '{:<20}'.format('[{0}]'.format(location)) + message )                         
        elif type == 'warning':
            self.logger.warning( '{:<38}'.format('{1}:WARNING [{0}]'.format(date,self.name)) + '{:<20}'.format('[{0}]'.format(location)) + message )   
        elif type == 'error':
            self.logger.error( '{:<38}'.format('{1}:ERROR [{0}]'.format(date,self.name)) +  '{:<20}'.format('[{0}]'.format(location)) + message )   

RTCLogger = RTCLogger()