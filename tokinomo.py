import sys
import os
import threading
from time import sleep
import logging
import argparse
from threading import Timer
import time

from device.board import Board
from device.controller import DeviceController
from networkmanager import NetworkManager
from devicelogger import DeviceLogger
from connectionlogger import ConnectionLogger
from usagelogger import UsageLogger
from status import Status
from network import Network
from const import Constants

class Tokinomo(object):

    ## Basic components initialization

    def __init__(self, clock):
        # init main components
        Status.initialize(clock)
        UsageLogger.initialize()
        DeviceController.initialize()
        self.nm = NetworkManager(self.on_connection_established, self.on_connection_error)
        self.network = Network()
         # stop the connect thread after 1 minute if unresponsive
        self.connect_timer = None

    def initialize(self):
        # initialize network manager
        self.nm.start()
        DeviceController.set_device_info()
        Status.set_initial(1)
        DeviceController.set_device_location_datetime()
        Status.set_modem_status(True)


    ## Network manager callback

    def on_connection_established(self, connection_type):
        # manage established connection to cloud  
        self.connect_timer = threading.Timer(180, self.force_connection_error)
        self.connect_timer.start()
        DeviceLogger.log('info', 'NetworkManager', 'Started Connection Thread Timer')
        self.nm.stop()
        ConnectionLogger.log('info', 'NetworkManager', 'Connection to cloud established via ' + connection_type)
        DeviceLogger.log('info', 'NetworkManager', 'Connection to cloud established via ' + connection_type)
        if connection_type == 'WiFi':
            DeviceController.set_device_connection(9)
        if connection_type == 'GPRS':      
            DeviceController.set_device_connection(10)  
        self.network.update(connection_type)
        debugmode = int(Status.get_major_setting('debugmode'))
        keepmodemon = int(Status.get_major_setting('keepmodemon'))
        if connection_type == 'GPRS':
            Status.set_netlight()
        if connection_type == 'WiFi':
            self.nm.set_timer(Constants.short_time) 
        if connection_type == 'GPRS' and (debugmode == 1 and keepmodemon == 1):
            self.nm.set_timer(Constants.short_time)
        if connection_type == 'GPRS' and (debugmode == 0 and keepmodemon == 1):
            self.nm.set_timer(Constants.long_time)
        if connection_type == 'GPRS' and (debugmode == 1 and keepmodemon == 0):
            self.nm.set_timer(Constants.short_time)
            self.nm.stop_modem() # -> Don't close pppd connection after connection to cloud
        if connection_type == 'GPRS' and (debugmode == 0 and keepmodemon == 0):
            self.nm.set_timer(Constants.long_time)
            self.nm.stop_modem() # -> Don't close pppd connection after connection to cloud
        if Status.check_standby_status():
            if Status.get_device_on() == True:
                Status.set_device_on(False)
            if debugmode == 0:
                time = Constants.long_time
                DeviceLogger.log('info', 'Tokinomo', 'Device on Standby; Checking for cloud in 3600 seconds ')
            else:
                time = Constants.short_time
                DeviceLogger.log('info', 'Tokinomo', 'Device on Standby; Checking for cloud in 30 seconds; Debug Mode On ')
            self.nm.set_timer(time)
        # if wifi is available and modem info has not been gathered overwrite timer to 7 seconds
        if Status.get_initial() == 0:
            self.nm.set_timer(7)
        self.connect_timer.cancel()
        # if timer already started by force connection error do not restart it
        if self.nm.get_pool_timer_alive():
            print('return from main')
            return
        self.nm.start_timer()
        DeviceLogger.log('info', 'NetworkManager', 'Canceled Connection Thread Timer')
        DeviceLogger.log('info', 'NetworkManager', 'NetworkManager Finished')
        DeviceLogger.log('info', 'NetworkManager', '.............END............')

    def force_connection_error(self):
        for thread in threading.enumerate(): 
            # if thread is active and Connection Thread timer is not started
            if 'Establish Connection Thread' == thread.name and not self.nm.get_pool_timer_alive():
                DeviceLogger.log('error', 'NetworkManager', 'Connection Thread unresponsive, forced stop')
                self.on_connection_error()
                return
        DeviceLogger.log('error', 'NetworkManager', 'Connection Thread unresponsive timer elapsed but no thread found')

    def on_connection_error(self):
        # manage connection to cloud not established  
        if self.connect_timer != None:
            self.connect_timer.cancel()
            DeviceLogger.log('info', 'NetworkManager', 'Canceled Connection Thread Timer')
        self.nm.stop()
        self.nm.stop_modem()
        if self.nm.get_pool_timer_alive():
            print('return from error')
            return
        self.nm.start_timer()
        ConnectionLogger.log('info', 'NetworkManager', 'Connection NOT established')
        DeviceLogger.log('info', 'NetworkManager', 'Connection NOT established')
        DeviceLogger.log('info', 'NetworkManager', 'NetworkManager Finished')
        DeviceLogger.log('info', 'NetworkManager', '.............END............')
        DeviceController.set_device_connection(8)
        
    ## Threads counter
    
    def threads_count_thread(self):
        # used for debugging: count and display active threads
        self.worker = threading.Thread(target=self.threads_count, name="Threads Counter Thread")
        self.worker.start()

    def threads_count(self):
        # used for debugging: count and display active threads
        while True:
            sleep(1)
            print('------------- Active threads: '+ str(threading.active_count()))
            print()
            for thread in threading.enumerate(): 
                print(thread.name)
            print('-------------')
     
 
if __name__ == '__main__':

    # added parser for detailed live logging    
    parser = argparse.ArgumentParser(description='Tokinomo Device App', add_help=True)
    parser.add_argument('-t', '--threads', action='store_true', help="log current number of threads every 1 sec")
    parser.add_argument('-v', '--version', action='store_true', help="check software version")
    parser.add_argument('-c', '--clock', action='store_true', help="block application from setting date from external RTC clock; useful for time/date issues ")
    args = parser.parse_args()

    if args.version:
        print('Device software version: ' + Constants.app_version)
        os._exit(0)

    DeviceLogger.log('info', 'Tokinomo', 'Starting Application')
    clock = True
    if args.clock:
        clock = False
    tknm = Tokinomo(clock)

    if args.threads:
        tknm.threads_count_thread()

    tknm.initialize()
